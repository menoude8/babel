#include "contactitem.h"

ContactItem::ContactItem(User *u, QWidget *parent) :
    QWidget(parent), user(u)
{
    setFixedHeight(40);
    QGridLayout *layout = new QGridLayout(this);

    avatar = new QLabel(this);
    if (u->avatar.isNull())
        avatar->setPixmap(resizePic(QPixmap(":/assets/default-avatar.png"), 40,40));
    else
        avatar->setPixmap(resizePic(QPixmap::fromImage(u->avatar), 40, 40));

    state = new QLabel(this);
    switch (u->etat)
    {
    case OffLine:
        state->setPixmap(resizePic(QPixmap(":/assets/offline.png"), 20,20));
    break;
    case OnLine:
        state->setPixmap(resizePic(QPixmap(":/assets/online.png"), 20,20));
    break;
    case Busy:
        state->setPixmap(resizePic(QPixmap(":/assets/busy.png"), 20,20));
    break;
    case Away:
        state->setPixmap(resizePic(QPixmap(":/assets/away.png"), 20,20));
    break;
    }
    login = new QLabel(u->log, this);
    login->setStyleSheet("font: 16px; color: black; font-weight:bold;");
    rang = new QLabel(u->rang, this);
    rang->setStyleSheet("font: 14px; color: gray; font-weight:bold;");
    login->setMinimumWidth(130);
    newMessage = new QLabel(this);
    apelManque = new QLabel(this);
    layout->addWidget(avatar, 0,0,2,1);
    layout->addWidget(state,0, 1, 1, 1);
    layout->addWidget(login, 0, 2, 1, 1);
    layout->addWidget(newMessage, 0, 3, 1, 1, Qt::AlignRight);
    layout->addWidget(rang, 1, 1, 1, 2);
    layout->addWidget(apelManque, 1, 3,1, 1, Qt::AlignRight);
    setLayout(layout);
    layout->setSizeConstraint( QLayout::SetFixedSize );
    layout->setMargin(3);

    this->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(const QPoint&)),
        this, SLOT(ShowContextMenu(const QPoint&)));
}
