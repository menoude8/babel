#ifndef CENTRALPAGE_H
#define CENTRALPAGE_H

#include <QtGui>
#include "rsslisting.h"
#include "acceuil.h"
#include "pluginlist.h"
#include "profil.h"

class centralPage : public QWidget
{
    Q_OBJECT
public:
    explicit centralPage(User *u, QWidget *parent = 0);

signals:
    void refrechBar();
    void modifyUser();
public slots:
    void modifyAccount()
    {
        homeClick();
 /*    User *tmp = user;
     user = u;
     user->etat = tmp->etat
     delete tmp;
     */
     modifyUser();
    }

    void homeClick()
    {
        stack->removeWidget(stack->widget(0));
        delete acceuil;
        acceuil = new Acceuil(this);
        stack->insertWidget(0, acceuil);
        stack->setCurrentIndex(0);
        profilBut->setEnabled(true);
        actuBut->setEnabled(true);
        homeBut->setEnabled(false);
        pluginBut->setEnabled(true);

    }
    void actuClick()
    {
        stack->setCurrentIndex(1);
        profilBut->setEnabled(true);
        actuBut->setEnabled(false);
        homeBut->setEnabled(true);
        pluginBut->setEnabled(true);

    }
    void profilClick()
    {
        stack->removeWidget(stack->widget(2));
        delete (stack->widget(2));
        Profil *l = new Profil(user, this);
        connect(l, SIGNAL(modifyAccount()), this, SLOT(modifyAccount()));
        stack->insertWidget(2, l);
        stack->setCurrentIndex(2);
        profilBut->setEnabled(false);
        actuBut->setEnabled(true);
        homeBut->setEnabled(true);
        pluginBut->setEnabled(true);
    }

    void pluginClick()
    {
        stack->removeWidget(stack->widget(3));
        delete (stack->widget(3));
        PluginList *l = new PluginList(this);
        stack->insertWidget(3, l);
        connect(l, SIGNAL(refrechBar()), this, SIGNAL(refrechBar()));
        stack->setCurrentIndex(3);
        profilBut->setEnabled(true);
        actuBut->setEnabled(true);
        homeBut->setEnabled(true);
        pluginBut->setEnabled(false);
    }

private:
    User *user;

    QPushButton *profilBut;
    QPushButton *homeBut;
    QPushButton *actuBut;
    QPushButton *pluginBut;
    QLabel *header;
    Acceuil *acceuil;
    QStackedWidget *stack;

};

#endif // CENTRALPAGE_H
