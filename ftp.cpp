#include "ftp.h"

Ftp::Ftp(QWidget *parent) :
    QWidget(parent)
{
    ftp = NULL;
   statusLabel = new QLabel();

       fileList = new QTreeWidget;
       fileList->setEnabled(false);
       fileList->setRootIsDecorated(false);
       fileList->setHeaderLabels(QStringList() << tr("Name") << tr("Size") << tr("Owner") << tr("Group") << tr("Time"));
       fileList->header()->setStretchLastSection(true);

       cdToParentButton = new QPushButton;
       cdToParentButton->setIcon(QPixmap(":/assets/parent.png"));
       cdToParentButton->setFixedSize(40, 30);
       cdToParentButton->setEnabled(false);

       downloadButton = new QPushButton(tr("Download"));
       downloadButton->setEnabled(false);
       downloadButton->setFixedSize(60, 30);
       progressDialog = new QProgressBar(this);
       progressDialog->setVisible(false);
       progressLabel = new QLabel(this);
       progressLabel->setVisible(false);
       connect(fileList, SIGNAL(itemActivated(QTreeWidgetItem*,int)),
               this, SLOT(processItem(QTreeWidgetItem*,int)));
       connect(fileList, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)),
               this, SLOT(enableDownloadButton()));
       connect(progressDialog, SIGNAL(canceled()), this, SLOT(cancelDownload()));
       connect(cdToParentButton, SIGNAL(clicked()), this, SLOT(cdToParent()));
       connect(downloadButton, SIGNAL(clicked()), this, SLOT(downloadFile()));

       QHBoxLayout *topLayout = new QHBoxLayout;
       topLayout->addWidget(cdToParentButton);
       topLayout->addWidget(downloadButton);
       QHBoxLayout *botLayout = new QHBoxLayout;
       botLayout->addWidget(progressLabel);
       botLayout->addWidget(progressDialog);

       QVBoxLayout *mainLayout = new QVBoxLayout;
       mainLayout->addLayout(topLayout);
       mainLayout->addWidget(fileList);
       mainLayout->addWidget(statusLabel);
       mainLayout->addLayout(botLayout);
       setLayout(mainLayout);
        connectOrDisconnect();
    }

   QSize Ftp::sizeHint() const
   {
       return QSize(500, 300);
   }

   void Ftp::connectOrDisconnect()
   {
       if (ftp) {
           ftp->abort();
           ftp->deleteLater();
           ftp = 0;
           fileList->setEnabled(false);
           cdToParentButton->setEnabled(false);
           downloadButton->setEnabled(false);
           return;
       }

       ftp = new QFtp(this);
       connect(ftp, SIGNAL(commandFinished(int,bool)),
               this, SLOT(ftpCommandFinished(int,bool)));
       connect(ftp, SIGNAL(listInfo(QUrlInfo)),
               this, SLOT(addToList(QUrlInfo)));
       connect(ftp, SIGNAL(dataTransferProgress(qint64,qint64)),
               this, SLOT(updateDataTransferProgress(qint64,qint64)));

       fileList->clear();
       currentPath.clear();
       isDirectory.clear();

       ftp->connectToHost("89.86.252.129");
       ftp->login("mkdex");
       ftp->cd("");

       fileList->setEnabled(true);
       statusLabel->setText(tr("Connecting to FTP server..."));
   }

   void Ftp::downloadFile()
   {
       QString fileName = fileList->currentItem()->text(0);

       if (QFile::exists( QDesktopServices::storageLocation(QDesktopServices::HomeLocation) + "/var/" + fileName) || QFile::exists( QDesktopServices::storageLocation(QDesktopServices::HomeLocation) + "/var/" + fileName.section('.', 0, 0))) {
           QMessageBox::information(this, tr("FTP"),
                                    tr("There already exists a file called %1 in "
                                       "the current directory.")
                                    .arg(fileName));
           return;
       }

       file = new QFile(QDesktopServices::storageLocation(QDesktopServices::HomeLocation)+"/var/"+fileName);
       if (!file->open(QIODevice::WriteOnly)) {
           QMessageBox::information(this, tr("FTP"),
                                    tr("Unable to save the file %1: %2.")
                                    .arg(fileName).arg(file->errorString()));
           delete file;
           return;
       }

       ftp->get(fileList->currentItem()->text(0), file);

       progressLabel->setText(tr("Downloading %1...").arg(fileName));
       downloadButton->setEnabled(false);
       progressDialog->setVisible(true);
       progressLabel->setVisible(true);
       }

   void Ftp::cancelDownload()
   {
       ftp->abort();
   }

   void Ftp::ftpCommandFinished(int, bool error)
   {
       if (ftp->currentCommand() == QFtp::ConnectToHost) {
           if (error) {
               QMessageBox::information(this, tr("FTP"),
                                        tr("Unable to connect to the FTP server "
                                           "."));
               connectOrDisconnect();
               return;
           }
           statusLabel->setText(tr("Logged."));
           fileList->setFocus();
           downloadButton->setDefault(true);
           return;
       }

       if (ftp->currentCommand() == QFtp::Login)
           ftp->list();

       if (ftp->currentCommand() == QFtp::Get) {
           if (error) {
               statusLabel->setText(tr("Canceled download of %1.")
                                    .arg(file->fileName()));
               file->close();
               file->remove();
           } else {

               QDir pluginsDir = QDir(qApp->applicationDirPath());
               pluginsDir.cd("plugins");

               statusLabel->setText(tr("File Downloaded."));
                QString cmd = "bin/unzip.exe " + file->fileName() + " -d " +pluginsDir.absolutePath();
                QString fileName = file->fileName();
                file->close();
                QProcess::execute(cmd);


                QFile::remove(fileName);
                newDLFile();
           }
           delete file;
           enableDownloadButton();
           progressDialog->setVisible(false);
           progressLabel->setVisible(false);
       } else if (ftp->currentCommand() == QFtp::List) {
           if (isDirectory.isEmpty()) {
               fileList->addTopLevelItem(new QTreeWidgetItem(QStringList() << tr("<empty>")));
               fileList->setEnabled(false);
           }
       }
    }

   void Ftp::addToList(const QUrlInfo &urlInfo)
   {
       QTreeWidgetItem *item = new QTreeWidgetItem;
       item->setText(0, urlInfo.name());
       item->setText(1, QString::number(urlInfo.size()));
       item->setText(2, urlInfo.owner());
       item->setText(3, urlInfo.group());
       item->setText(4, urlInfo.lastModified().toString("MMM dd yyyy"));

       QPixmap pixmap(urlInfo.isDir() ? ":/assets/dir.png" : ":/assets/file.png");
       item->setIcon(0, pixmap);

       isDirectory[urlInfo.name()] = urlInfo.isDir();
       fileList->addTopLevelItem(item);
       if (!fileList->currentItem()) {
           fileList->setCurrentItem(fileList->topLevelItem(0));
           fileList->setEnabled(true);
       }
   }

   void Ftp::processItem(QTreeWidgetItem *item, int /*column*/)
   {
       QString name = item->text(0);
       if (isDirectory.value(name)) {
           fileList->clear();
           isDirectory.clear();
           currentPath += '/';
           currentPath += name;
           ftp->cd(name);
           ftp->list();
           cdToParentButton->setEnabled(true);
           return;
       }
   }

   void Ftp::cdToParent()
   {
       fileList->clear();
       isDirectory.clear();
       currentPath = currentPath.left(currentPath.lastIndexOf('/'));
       if (currentPath.isEmpty()) {
           cdToParentButton->setEnabled(false);
           ftp->cd("/");
       } else {
           ftp->cd(currentPath);
       }
       ftp->list();
   }

   void Ftp::updateDataTransferProgress(qint64 readBytes,
                                              qint64 totalBytes)
   {
       progressDialog->setMaximum(totalBytes);
       progressDialog->setValue(readBytes);
   }

   void Ftp::enableDownloadButton()
   {
       QTreeWidgetItem *current = fileList->currentItem();
       if (current) {
           QString currentFile = current->text(0);
           downloadButton->setEnabled(!isDirectory.value(currentFile));
       } else {
           downloadButton->setEnabled(false);
       }
   }
