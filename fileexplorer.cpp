#include "fileexplorer.h"

fileExplorer::fileExplorer(QWidget *parent) :
    QWidget(parent)
{


    setFixedSize(800, 600);
    setWindowTitle("Explorer");
    setWindowIcon(QIcon(":/assets/icon.png"));
    enter = new QPushButton("Load", this);
    enter->setEnabled(false);
    back = new QPushButton("Back", this);
    back->setShortcut(Qt::Key_Back);
    connect(back, SIGNAL(clicked()), this, SLOT(GoToParent()));

    cancel = new QPushButton("Cancel", this);
    QGridLayout *lay = new QGridLayout(this);

    arbre = new QTreeView(this);
    table = new QListView(this);
    model = new QDirModel(this);
    modelDir = new QDirModel(this);
    modelDir->setFilter(QDir::Dirs | QDir::Drives);
    //arbre->setViewMode(QListView::ListMode);
    table->setResizeMode(QListView::Adjust);
     table->setMovement(QListView::Static);
    table->setViewMode(QListView::IconMode);
    table->setSpacing(10);
    table->setIconSize(QSize(150,150));
    table->setUniformItemSizes(true);
    table->setWrapping(true);
    table->setLayoutMode(QListView::Batched);
  //  table->setSelectionMode(QAbstractItemView::ExtendedSelection);

    CurPath = new QDir();
    CurPath->cd(CurPath->homePath());
    path = new QLineEdit(CurPath->path(), this);

    if (CurPath->isRoot())
        back->setEnabled(false);
    table->setModel(model);
    arbre->setModel(modelDir);
    arbre->setFixedWidth(200);
    table->setFixedWidth(580);
    table->setRootIndex(model->index(CurPath->path()));
    arbre->setCurrentIndex(modelDir->index(CurPath->path()));
    name = new QLineEdit(this);
    name->setReadOnly(true);
    connect(table,SIGNAL(doubleClicked(QModelIndex)), this,SLOT(goToList(QModelIndex)));
    connect(arbre,SIGNAL(clicked(QModelIndex)), this,SLOT(goToTree(QModelIndex)));
    connect(table,SIGNAL(activated(QModelIndex)), this,SLOT(goToList(QModelIndex)));
    connect(arbre,SIGNAL(activated(QModelIndex)), this,SLOT(goToTree(QModelIndex)));
    connect(cancel, SIGNAL(clicked()), this, SLOT(close()));
    connect(enter, SIGNAL(clicked()), this, SLOT(sendPath()));
    connect(path, SIGNAL(returnPressed()), this, SLOT(goTo()));
    lay->addWidget(back, 0, 0, 1, 1);
    lay->addWidget(path, 0, 1, 1, 1);

    lay->addWidget(table, 1, 1, 1, 2);
    lay->addWidget(arbre, 1, 0, 1, 1);
    lay->addWidget(name, 2, 1, 1, 2);
    lay->addWidget(enter, 3, 1, 1, 1);
    lay->addWidget(cancel, 3, 2, 1, 1);
    connect(table, SIGNAL(pressed(QModelIndex)), this, SLOT(upButton(QModelIndex)));

    setLayout(lay);

}

void fileExplorer::upButton(QModelIndex index)
{
    QFileInfo info(model->filePath(index));
    if (info.isDir())
    {
        enter->setEnabled(false);
        name->setText("");
    }
    else
    {
        name->setText(info.fileName());
        enter->setEnabled(true);
    }
}

void fileExplorer::goTo()
{
    QFileInfo info(path->text());
    if (info.isDir())
    {
        CurPath->cd(path->text());
        table->setRootIndex(model->index(CurPath->path()));
        arbre->setCurrentIndex(modelDir->index(CurPath->path()));
        back->setEnabled(true);
        path->setText(CurPath->path());
    }
    upButton(model->index(CurPath->path()));

}

void fileExplorer::goToList(QModelIndex index)
{
    QFileInfo info(model->filePath(index));
    if (info.isDir())
    {
        CurPath->cd(model->filePath(index));
        table->setRootIndex(model->index(CurPath->path()));
        arbre->setCurrentIndex(modelDir->index(CurPath->path()));
        back->setEnabled(true);
        path->setText(CurPath->path());
    }
    upButton(index);
}

void fileExplorer::goToTree(QModelIndex index)
{

    CurPath->cd(modelDir->filePath(index));
    table->setRootIndex(model->index(CurPath->path()));
    arbre->setCurrentIndex(modelDir->index(CurPath->path()));
    back->setEnabled(true);
    path->setText(CurPath->path());
}

void fileExplorer::GoToParent()
{

    CurPath->cdUp();
    path->setText(CurPath->path());
    table->setRootIndex(model->index(CurPath->path()));
    upButton(model->index(CurPath->path()));
    arbre->setCurrentIndex(modelDir->index(CurPath->path()));
    if (CurPath->isRoot())
        back->setEnabled(false);
}

void fileExplorer::sendPath()
{
    fileSelected(getPath());
    name->setText("");
    close();
}


