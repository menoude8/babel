#include "addcontact.h"

AddContact::AddContact(QWidget *parent) :
    QWidget(parent)
{
    setWindowFlags(Qt::Popup | Qt::Window);
    setObjectName("addContact");
    QGridLayout *lay = new QGridLayout(this);
    titre = new QLabel("Ajouter un contact", this);
    login = new QLineEdit(this);
    connect(login, SIGNAL(textEdited(QString)), this, SLOT(refreshBut()));
    ok = new QPushButton("Ajouter", this);
    cancel = new QPushButton("Annuler", this);
    connect(ok, SIGNAL(clicked()), this, SLOT(bottonOkClick()));
    connect(cancel, SIGNAL(clicked()), this, SIGNAL(Cancel()));

    lay->addWidget(titre , 0, 0, 1, 2);
    lay->addWidget(login , 1, 0, 1, 2);
    lay->addWidget(ok , 2, 0, 1, 1);
    lay->addWidget(cancel , 2, 1, 1, 1);

    setLayout(lay);
}
