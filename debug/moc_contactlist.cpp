/****************************************************************************
** Meta object code from reading C++ file 'contactlist.h'
**
** Created: Sun 9. Dec 05:39:34 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../contactlist.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'contactlist.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ContactList[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x05,
      31,   12,   12,   12, 0x05,
      51,   12,   12,   12, 0x05,
      70,   12,   12,   12, 0x05,
      91,   12,   12,   12, 0x05,
     106,   12,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
     133,  129,   12,   12, 0x0a,
     157,  155,   12,   12, 0x0a,
     175,   12,   12,   12, 0x0a,
     189,  155,   12,   12, 0x0a,
     213,  210,   12,   12, 0x0a,
     235,  232,   12,   12, 0x0a,
     256,   12,   12,   12, 0x0a,
     267,   12,   12,   12, 0x0a,
     279,  129,   12,   12, 0x0a,
     302,   12,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ContactList[] = {
    "ContactList\0\0callSignal(User*)\0"
    "screenSignal(User*)\0statusChanged(int)\0"
    "rangChanged(QString)\0addChat(User*)\0"
    "inviteContact(QString)\0log\0"
    "refreshState(QString)\0c\0addContact(User*)\0"
    "refreshUser()\0modifyContact(User*)\0"
    "st\0refreshStatus(int)\0id\0newChat(QModelIndex)\0"
    "addClick()\0addCancel()\0askForContact(QString)\0"
    "setRang()\0"
};

const QMetaObject ContactList::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_ContactList,
      qt_meta_data_ContactList, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ContactList::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ContactList::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ContactList::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ContactList))
        return static_cast<void*>(const_cast< ContactList*>(this));
    return QWidget::qt_metacast(_clname);
}

int ContactList::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: callSignal((*reinterpret_cast< User*(*)>(_a[1]))); break;
        case 1: screenSignal((*reinterpret_cast< User*(*)>(_a[1]))); break;
        case 2: statusChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: rangChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: addChat((*reinterpret_cast< User*(*)>(_a[1]))); break;
        case 5: inviteContact((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: refreshState((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 7: addContact((*reinterpret_cast< User*(*)>(_a[1]))); break;
        case 8: refreshUser(); break;
        case 9: modifyContact((*reinterpret_cast< User*(*)>(_a[1]))); break;
        case 10: refreshStatus((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: newChat((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 12: addClick(); break;
        case 13: addCancel(); break;
        case 14: askForContact((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 15: setRang(); break;
        default: ;
        }
        _id -= 16;
    }
    return _id;
}

// SIGNAL 0
void ContactList::callSignal(User * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ContactList::screenSignal(User * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ContactList::statusChanged(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void ContactList::rangChanged(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void ContactList::addChat(User * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void ContactList::inviteContact(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_END_MOC_NAMESPACE
