/****************************************************************************
** Meta object code from reading C++ file 'addvar.h'
**
** Created: Tue 22. May 10:02:16 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../addvar.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'addvar.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_addVar[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
       8,    7,    7,    7, 0x05,
      17,   15,    7,    7, 0x05,

 // slots: signature, parameters, type, tag, flags
      39,   35,    7,    7, 0x0a,
      54,    7,    7,    7, 0x0a,
      65,   60,    7,    7, 0x0a,
      80,    7,    7,    7, 0x0a,
      89,    7,    7,    7, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_addVar[] = {
    "addVar\0\0Back()\0v\0Success(variete*)\0"
    "val\0indicaDys(int)\0Add()\0path\0"
    "Added(QString)\0Cancel()\0Save()\0"
};

const QMetaObject addVar::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_addVar,
      qt_meta_data_addVar, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &addVar::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *addVar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *addVar::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_addVar))
        return static_cast<void*>(const_cast< addVar*>(this));
    return QWidget::qt_metacast(_clname);
}

int addVar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: Back(); break;
        case 1: Success((*reinterpret_cast< variete*(*)>(_a[1]))); break;
        case 2: indicaDys((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: Add(); break;
        case 4: Added((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: Cancel(); break;
        case 6: Save(); break;
        default: ;
        }
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void addVar::Back()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void addVar::Success(variete * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
