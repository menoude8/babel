/****************************************************************************
** Meta object code from reading C++ file 'addrss.h'
**
** Created: Sun 9. Dec 05:39:21 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../addrss.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'addrss.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_addRSS[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      10,    8,    7,    7, 0x05,

 // slots: signature, parameters, type, tag, flags
      35,    7,    7,    7, 0x0a,
      44,    7,    7,    7, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_addRSS[] = {
    "addRSS\0\0,\0addFlux(QString,QString)\0"
    "Enable()\0send()\0"
};

const QMetaObject addRSS::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_addRSS,
      qt_meta_data_addRSS, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &addRSS::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *addRSS::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *addRSS::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_addRSS))
        return static_cast<void*>(const_cast< addRSS*>(this));
    return QWidget::qt_metacast(_clname);
}

int addRSS::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: addFlux((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 1: Enable(); break;
        case 2: send(); break;
        default: ;
        }
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void addRSS::addFlux(QString _t1, QString _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
