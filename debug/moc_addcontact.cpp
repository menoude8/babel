/****************************************************************************
** Meta object code from reading C++ file 'addcontact.h'
**
** Created: Sun 9. Dec 05:39:54 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../addcontact.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'addcontact.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_AddContact[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x05,
      21,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
      34,   11,   11,   11, 0x0a,
      50,   11,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_AddContact[] = {
    "AddContact\0\0Cancel()\0Add(QString)\0"
    "bottonOkClick()\0refreshBut()\0"
};

const QMetaObject AddContact::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_AddContact,
      qt_meta_data_AddContact, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AddContact::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AddContact::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AddContact::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AddContact))
        return static_cast<void*>(const_cast< AddContact*>(this));
    return QWidget::qt_metacast(_clname);
}

int AddContact::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: Cancel(); break;
        case 1: Add((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: bottonOkClick(); break;
        case 3: refreshBut(); break;
        default: ;
        }
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void AddContact::Cancel()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void AddContact::Add(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
