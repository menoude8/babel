/****************************************************************************
** Meta object code from reading C++ file 'fileexplorer.h'
**
** Created: Sun 9. Dec 05:39:13 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../fileexplorer.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'fileexplorer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_fileExplorer[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      19,   14,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      41,   13,   13,   13, 0x0a,
      63,   13,   13,   13, 0x0a,
      85,   13,   13,   13, 0x0a,
      92,   13,   13,   13, 0x0a,
     105,   13,   13,   13, 0x0a,
     116,   13,   13,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_fileExplorer[] = {
    "fileExplorer\0\0path\0fileSelected(QString)\0"
    "goToTree(QModelIndex)\0goToList(QModelIndex)\0"
    "goTo()\0GoToParent()\0sendPath()\0"
    "upButton(QModelIndex)\0"
};

const QMetaObject fileExplorer::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_fileExplorer,
      qt_meta_data_fileExplorer, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &fileExplorer::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *fileExplorer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *fileExplorer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_fileExplorer))
        return static_cast<void*>(const_cast< fileExplorer*>(this));
    return QWidget::qt_metacast(_clname);
}

int fileExplorer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: fileSelected((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: goToTree((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 2: goToList((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 3: goTo(); break;
        case 4: GoToParent(); break;
        case 5: sendPath(); break;
        case 6: upButton((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void fileExplorer::fileSelected(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
