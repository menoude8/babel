/****************************************************************************
** Meta object code from reading C++ file 'calludp.h'
**
** Created: Sun 9. Dec 19:05:54 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../calludp.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'calludp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CallUdp[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
       9,    8,    8,    8, 0x05,

 // slots: signature, parameters, type, tag, flags
      18,    8,    8,    8, 0x0a,
      30,    8,    8,    8, 0x0a,
      40,    8,    8,    8, 0x0a,
      51,    8,    8,    8, 0x0a,
      63,    8,    8,    8, 0x0a,
      74,    8,    8,    8, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_CallUdp[] = {
    "CallUdp\0\0hangUp()\0newClient()\0reponse()\0"
    "stopCall()\0startCall()\0sendFram()\0"
    "ping()\0"
};

const QMetaObject CallUdp::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_CallUdp,
      qt_meta_data_CallUdp, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CallUdp::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CallUdp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CallUdp::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CallUdp))
        return static_cast<void*>(const_cast< CallUdp*>(this));
    return QObject::qt_metacast(_clname);
}

int CallUdp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: hangUp(); break;
        case 1: newClient(); break;
        case 2: reponse(); break;
        case 3: stopCall(); break;
        case 4: startCall(); break;
        case 5: sendFram(); break;
        case 6: ping(); break;
        default: ;
        }
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void CallUdp::hangUp()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
