/****************************************************************************
** Meta object code from reading C++ file 'dialserv.h'
**
** Created: Sun 9. Dec 05:39:26 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../dialserv.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dialserv.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DialServ[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      33,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      14,       // signalCount

 // signals: signature, parameters, type, tag, flags
      22,   10,    9,    9, 0x05,
      68,   55,    9,    9, 0x05,
     115,  106,    9,    9, 0x05,
     159,  149,    9,    9, 0x05,
     198,    9,    9,    9, 0x05,
     220,    9,    9,    9, 0x05,
     240,  238,    9,    9, 0x05,
     258,  238,    9,    9, 0x05,
     279,    9,    9,    9, 0x05,
     298,    9,    9,    9, 0x05,
     316,  314,    9,    9, 0x05,
     333,  331,    9,    9, 0x05,
     357,  314,    9,    9, 0x05,
     379,  377,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
     412,    9,    9,    9, 0x0a,
     436,  331,    9,    9, 0x0a,
     465,    9,    9,    9, 0x0a,
     490,  331,    9,    9, 0x0a,
     520,    9,    9,    9, 0x0a,
     538,    9,    9,    9, 0x0a,
     554,  552,    9,    9, 0x0a,
     570,    9,    9,    9, 0x0a,
     589,    9,    9,    9, 0x0a,
     605,    9,    9,    9, 0x0a,
     616,    9,    9,    9, 0x0a,
     636,  629,    9,    9, 0x0a,
     683,  679,    9,    9, 0x0a,
     709,  706,    9,    9, 0x0a,
     730,  727,    9,    9, 0x0a,
     750,    9,    9,    9, 0x0a,
     763,  331,    9,    9, 0x0a,
     788,    9,    9,    9, 0x0a,
     800,    9,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_DialServ[] = {
    "DialServ\0\0log,ip,port\0"
    "newCall(QString,QString,quint16)\0"
    "log,ip,port,\0repCall(QString,QString,quint16,bool)\0"
    ",ip,port\0newShare(QString,QString,quint16)\0"
    ",ip,port,\0repShare(QString,QString,quint16,bool)\0"
    "connectedToServ(bool)\0showText(QString)\0"
    "u\0addContact(User*)\0modifyContact(User*)\0"
    "repForLoging(bool)\0connected(bool)\0c\0"
    "addChat(User*)\0,\0newMsg(QString,QString)\0"
    "stateChanged(User&)\0r\0"
    "searchResult(std::vector<User*>)\0"
    "askForCallSlot(QString)\0"
    "repForCallSlot(QString,bool)\0"
    "askForShareSlot(QString)\0"
    "repForShareSlot(QString,bool)\0"
    "startConnection()\0addUser(User)\0l\0"
    "search(QString)\0askForLog(QString)\0"
    "donneesRecues()\0connecte()\0deconnecte()\0"
    "erreur\0erreurSocket(QAbstractSocket::SocketError)\0"
    "l,p\0login(QString,QString)\0st\0"
    "changeStatus(int)\0rg\0changeRang(QString)\0"
    "changeUser()\0sendMsg(QString,QString)\0"
    "publicate()\0inviteContact(QString)\0"
};

const QMetaObject DialServ::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_DialServ,
      qt_meta_data_DialServ, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DialServ::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DialServ::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DialServ::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DialServ))
        return static_cast<void*>(const_cast< DialServ*>(this));
    return QObject::qt_metacast(_clname);
}

int DialServ::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: newCall((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< quint16(*)>(_a[3]))); break;
        case 1: repCall((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< quint16(*)>(_a[3])),(*reinterpret_cast< bool(*)>(_a[4]))); break;
        case 2: newShare((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< quint16(*)>(_a[3]))); break;
        case 3: repShare((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< quint16(*)>(_a[3])),(*reinterpret_cast< bool(*)>(_a[4]))); break;
        case 4: connectedToServ((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: showText((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: addContact((*reinterpret_cast< User*(*)>(_a[1]))); break;
        case 7: modifyContact((*reinterpret_cast< User*(*)>(_a[1]))); break;
        case 8: repForLoging((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: connected((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: addChat((*reinterpret_cast< User*(*)>(_a[1]))); break;
        case 11: newMsg((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 12: stateChanged((*reinterpret_cast< User(*)>(_a[1]))); break;
        case 13: searchResult((*reinterpret_cast< std::vector<User*>(*)>(_a[1]))); break;
        case 14: askForCallSlot((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 15: repForCallSlot((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 16: askForShareSlot((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 17: repForShareSlot((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 18: startConnection(); break;
        case 19: addUser((*reinterpret_cast< User(*)>(_a[1]))); break;
        case 20: search((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 21: askForLog((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 22: donneesRecues(); break;
        case 23: connecte(); break;
        case 24: deconnecte(); break;
        case 25: erreurSocket((*reinterpret_cast< QAbstractSocket::SocketError(*)>(_a[1]))); break;
        case 26: login((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 27: changeStatus((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 28: changeRang((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 29: changeUser(); break;
        case 30: sendMsg((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 31: publicate(); break;
        case 32: inviteContact((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 33;
    }
    return _id;
}

// SIGNAL 0
void DialServ::newCall(QString _t1, QString _t2, quint16 _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void DialServ::repCall(QString _t1, QString _t2, quint16 _t3, bool _t4)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void DialServ::newShare(QString _t1, QString _t2, quint16 _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void DialServ::repShare(QString _t1, QString _t2, quint16 _t3, bool _t4)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void DialServ::connectedToServ(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void DialServ::showText(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void DialServ::addContact(User * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void DialServ::modifyContact(User * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void DialServ::repForLoging(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void DialServ::connected(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void DialServ::addChat(User * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void DialServ::newMsg(QString _t1, QString _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void DialServ::stateChanged(User & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}

// SIGNAL 13
void DialServ::searchResult(std::vector<User*> _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}
QT_END_MOC_NAMESPACE
