/****************************************************************************
** Meta object code from reading C++ file 'callbar.h'
**
** Created: Sun 9. Dec 16:47:47 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../callbar.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'callbar.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CallBar[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: signature, parameters, type, tag, flags
       9,    8,    8,    8, 0x05,
      23,    8,    8,    8, 0x05,
      36,    8,    8,    8, 0x05,
      48,    8,    8,    8, 0x05,
      61,    8,    8,    8, 0x05,
      77,    8,    8,    8, 0x05,

 // slots: signature, parameters, type, tag, flags
      92,    8,    8,    8, 0x0a,
     104,    8,    8,    8, 0x0a,
     122,    8,  117,    8, 0x0a,
     139,  137,    8,    8, 0x0a,
     158,    8,  117,    8, 0x0a,
     170,  137,    8,    8, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_CallBar[] = {
    "CallBar\0\0optionClick()\0startShare()\0"
    "stopShare()\0muteSignal()\0muteNotSignal()\0"
    "hangUpSignal()\0muteClick()\0shareClick()\0"
    "bool\0getIsSharing()\0s\0setIsSharing(bool)\0"
    "getIsMute()\0setIsMute(bool)\0"
};

const QMetaObject CallBar::staticMetaObject = {
    { &QGroupBox::staticMetaObject, qt_meta_stringdata_CallBar,
      qt_meta_data_CallBar, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CallBar::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CallBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CallBar::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CallBar))
        return static_cast<void*>(const_cast< CallBar*>(this));
    return QGroupBox::qt_metacast(_clname);
}

int CallBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGroupBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: optionClick(); break;
        case 1: startShare(); break;
        case 2: stopShare(); break;
        case 3: muteSignal(); break;
        case 4: muteNotSignal(); break;
        case 5: hangUpSignal(); break;
        case 6: muteClick(); break;
        case 7: shareClick(); break;
        case 8: { bool _r = getIsSharing();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 9: setIsSharing((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: { bool _r = getIsMute();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 11: setIsMute((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void CallBar::optionClick()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void CallBar::startShare()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void CallBar::stopShare()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void CallBar::muteSignal()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void CallBar::muteNotSignal()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}

// SIGNAL 5
void CallBar::hangUpSignal()
{
    QMetaObject::activate(this, &staticMetaObject, 5, 0);
}
QT_END_MOC_NAMESPACE
