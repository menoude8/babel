/****************************************************************************
** Meta object code from reading C++ file 'apluginwidget.h'
**
** Created: Mon 26. Nov 17:37:39 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../apluginwidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'apluginwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_APluginWidget[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_APluginWidget[] = {
    "APluginWidget\0"
};

const QMetaObject APluginWidget::staticMetaObject = {
    { &IPluginWidget::staticMetaObject, qt_meta_stringdata_APluginWidget,
      qt_meta_data_APluginWidget, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &APluginWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *APluginWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *APluginWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_APluginWidget))
        return static_cast<void*>(const_cast< APluginWidget*>(this));
    if (!strcmp(_clname, "Epitech.Babel.Plugin.IPluginWidget/1.0"))
        return static_cast< IPluginWidget*>(const_cast< APluginWidget*>(this));
    return IPluginWidget::qt_metacast(_clname);
}

int APluginWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = IPluginWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
