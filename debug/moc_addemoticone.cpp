/****************************************************************************
** Meta object code from reading C++ file 'addemoticone.h'
**
** Created: Tue 27. Mar 12:27:18 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../addemoticone.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'addemoticone.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_addEmoticone[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   14,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      37,   13,   13,   13, 0x0a,
      44,   13,   13,   13, 0x0a,
      54,   13,   13,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_addEmoticone[] = {
    "addEmoticone\0\0,\0add(QString,QString)\0"
    "send()\0setPath()\0Enable()\0"
};

const QMetaObject addEmoticone::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_addEmoticone,
      qt_meta_data_addEmoticone, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &addEmoticone::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *addEmoticone::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *addEmoticone::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_addEmoticone))
        return static_cast<void*>(const_cast< addEmoticone*>(this));
    return QWidget::qt_metacast(_clname);
}

int addEmoticone::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: add((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 1: send(); break;
        case 2: setPath(); break;
        case 3: Enable(); break;
        default: ;
        }
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void addEmoticone::add(QString _t1, QString _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
