#include "callconfirmation.h"

CallConfirmation::CallConfirmation(QString log, QString i, quint16 p, QWidget *parent) :
    QDialog(parent), ip(i), port(p)
{
    setWindowFlags(Qt::Popup | Qt::Window);
    QGridLayout *layout = new QGridLayout(this);
    ok = new QPushButton("Accepter", this);
    ko = new QPushButton("Refuser", this);
    okWithVideo = new QPushButton("Accepter et partager la webcam", this);
    title =  new QLabel(log + " souhaite vous joindre.", this);
    connect(ok, SIGNAL(clicked()), this, SLOT(acceptSlot()));
    connect(okWithVideo, SIGNAL(clicked()), this, SLOT(acceptWVSlot()));
    connect(ko, SIGNAL(clicked()), this, SLOT(refuseSlot()));

    layout->addWidget(title, 0, 0, 1, 3, Qt::AlignHCenter);
    layout->addWidget(ok, 1, 0, 1, 1, Qt::AlignRight);
    layout->addWidget(okWithVideo, 1, 1, 1, 1, Qt::AlignLeft);
    layout->addWidget(ko, 1, 2, 1, 1, Qt::AlignHCenter);
    setLayout(layout);
}
