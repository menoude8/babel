#include "filereceiver.h"
#include <iostream>

fileReceiver::fileReceiver(QString i, quint16 p, QString n, QString log, QWidget * parent)
    :QWidget(parent), port(p), name(n), ip(i)
{
    explorer = new fileExplorer();
    connect(explorer, SIGNAL(fileSelected()), this, SLOT(pathSelected()));
    size = 0;
    sock = new QTcpSocket();
    stack = new QStackedWidget(this);
    layout1 = new QGridLayout(this);
    label1 = new QLabel(log + " want to send you a file. Do you want download " + name +"?");
    accept = new QPushButton("accept", this);
    refuse = new QPushButton("refuse", this);
    connect(accept, SIGNAL(clicked()), this, SLOT(acceptFile()));
    connect(refuse, SIGNAL(clicked()), this, SLOT(refuseFile()));

    layout1->addWidget(label1, 0, 0, 1, 1, Qt::AlignCenter);
    layout1->addWidget(accept, 1, 0, 1, 1);
    layout1->addWidget(refuse, 1, 1, 1, 1);


    layout2 = new QGridLayout(this);
    bar = new QProgressBar(this);
    cancel = new QPushButton("cancel", this);
    label2 = new QLabel("starting...");
    layout2->addWidget(label2, 0, 0, 1, 1, Qt::AlignCenter);
    layout2->addWidget(bar, 1, 0, 1, 1);
    layout2->addWidget(cancel, 1, 1, 1, 1);

    QWidget *w1 = new QWidget(this);
    QWidget *w2 = new QWidget(this);
    w1->setLayout(layout1);
    w2->setLayout(layout2);
    stack->addWidget(w1);
    stack->addWidget(w2);

    mainLayout = new QGridLayout(this);
    mainLayout->addWidget(stack);
    setLayout(mainLayout);
}

fileReceiver::~fileReceiver()
{
}

void fileReceiver::acceptFile()
{
    sock->abort();
    qDebug()<<ip<<QString::number(port);
    sock->connectToHost(ip, port);

    explorer->show();

}

void fileReceiver::pathSelected()
{
    explorer->close();
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    out << OK;
    sock->write(block);
    stack->setCurrentIndex(1);
    connect(sock, SIGNAL(readyRead()), this, SLOT(donneesRecues()));
}

void fileReceiver::donneesRecues()
{
    QString path = explorer->getPath() + '/' + name;

    QDataStream in(sock);
    if (size == 0)
    {
           if (sock->bytesAvailable() < (int)sizeof(quint64))
                return;
           in >> size;
    }
    if (sock->bytesAvailable() < (int)size)
           return;





    /*char *bytes = (char *)malloc(4096);
    while (fil.size() < (int)size)
    {
        qDebug(QString::number(in.readRawData(bytes, 4096)).toAscii());
        fil.write(bytes);
    }*/

    th = new receiverThread(path, size, sock, this);


 sock->moveToThread(th);
 connect(th, SIGNAL(refresh(int)), this, SLOT(changeValue(int)));
 connect(th, SIGNAL(refreshBar(int)), bar, SLOT(setValue(int)));
 connect(th, SIGNAL(end()), this, SLOT(receptionOK()));



}

void fileReceiver::changeValue(int v)
{

//    valueChanged(v*100/size);
    int g, m, k, o;
    int gt, mt, kt, ot;
    int total = size;

    gt = total/1073741824;
    total = total%1073741824;

    mt = total/1048576;
    total = total%1048576;

    kt = total/1024;
    ot = total%1024;

    g = v/1073741824;
    v = v%1073741824;

    m = v/1048576;
    v = v%1048576;

    k = v/1024;
    o = v%1024;

    QString str = "";
    if (g > 0)
        str += QString::number(g) + "go";
    if (m > 0)
        str += QString::number(m) + "mo";
    if (k > 0)
        str += QString::number(k) + "ko";
    str += QString::number(o) + "o/";
    if (gt > 0)
        str += QString::number(gt) + "go";
    if (mt > 0)
        str += QString::number(mt) + "mo";
    if (kt > 0)
        str += QString::number(kt) + "ko";
    str += QString::number(ot) + "o.";
    label2->setText(str);
}

void fileReceiver::receptionOK()
{
    delete th;
    end();
}

void fileReceiver::refuseFile()
{
    sock->abort();
    sock->connectToHost(ip, port);
    QByteArray block;
        QDataStream out(&block, QIODevice::WriteOnly);
        out.setVersion(QDataStream::Qt_4_0);
        out << KO;
        sock->write(block);
        end();
}
