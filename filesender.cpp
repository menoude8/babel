#include "filesender.h"
#include <QFile>
#include <QMessageBox>
#include <QObject>

fileSender::fileSender(QString l, QWidget* parent)
    :    QWidget(parent), login(l)
{


    explorer = new fileExplorer();
    Path = new QLineEdit(this);
    Path->setReadOnly(true);
    Browse = new QPushButton(QIcon(":/assets/attachment.png"),"Choisir un fichier", this);
    sendFile = new QPushButton("envoyer", this);
    sendFile->setEnabled(false);

    connect(Browse, SIGNAL(clicked()), this, SLOT(selectFile()));
    connect(sendFile, SIGNAL(clicked()), this, SLOT(sendF()));
    connect(explorer, SIGNAL(fileSelected()), this, SLOT(fileSelected()));
    layout = new QGridLayout(this);
    layout->addWidget( Path, 0, 0, 1, 1);
    layout->addWidget( Browse, 0, 1, 1, 1);
    layout->addWidget( sendFile, 0, 2, 1, 1);
    setLayout(layout);
}

fileSender::~fileSender()
{

}

void fileSender::selectFile()
{
    explorer->show();
}

void fileSender::fileSelected()
{

    Path->setText(explorer->getPath());
    explorer->close();
    sendFile->setEnabled(true);
}

void fileSender::sendF()
{
    writeOnScreen("ask for sending file: " + Path->text());
    sock = new QTcpServer(this);
    if (!sock->listen()) {
             QMessageBox::critical(this,tr("File Server"),
                                   tr("Unable to send file: %1.")
                                   .arg(sock->errorString()));
             return;
         }
    sendFile->setEnabled(false);

    connect(sock, SIGNAL(newConnection()), this, SLOT(newClient()));
    sendItem(Path->text(), login, sock->serverPort());
    qDebug("test");
}

void fileSender::newClient()
{
    clientConnection = sock->nextPendingConnection();
    connect(clientConnection, SIGNAL(readyRead()), this, SLOT(reponse()));
    connect(clientConnection, SIGNAL(disconnected()), clientConnection, SLOT(deleteLater()));
}

void fileSender::reponse()
{
    QDataStream in(clientConnection);

    quint16 rep;
    in >> rep;
    if (rep == OK)
        sendFil();
    else
        return;
}

void fileSender::sendFil()
{
    writeOnScreen("sending file...");
    QFile file(Path->text());
    if (!file.open(QIODevice::ReadOnly))
               QMessageBox::information(this, tr("File"),tr("File cannot be opened."));
    QByteArray paquet;
    QDataStream out(&paquet, QIODevice::WriteOnly);
    out << (quint64) file.size();
    QByteArray c;
    clientConnection->write(paquet);

    while (!file.atEnd())
    {
        c = file.read(4096);
        clientConnection->write(c);
    }
    Path->setText("");
    sendFile->setEnabled(true);
    writeOnScreen("file sended");
}
