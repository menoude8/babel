#include "calludp.h"

#include <QApplication>
#include <QDesktopWidget>
#include "calludp.h"
#include "Protocole.h"

CallUdp::CallUdp(QString i, quint16 p, QObject *parent) :
    QObject(parent)
{
    ip = i;
    port = p;
    timer = new QTimer();
    sockIn = new QUdpSocket();
    sockOut = new QUdpSocket();
    qDebug() << "BIND: " << sockIn->bind(QHostAddress(i), p,QUdpSocket::ReuseAddressHint);
    connect(sockIn, SIGNAL(readyRead()), this, SLOT(reponse()));
    connect(timer, SIGNAL(timeout()), this, SLOT(startShare()));
    timer->start(1000);

    connect(&timerOut, SIGNAL(timeout()), this, SIGNAL(hangUp()));
    connect(&timerPing, SIGNAL(timeout()), this, SLOT(ping()));

    timerPing.start(5000);
    timerOut.start(10000);
}

void CallUdp::newClient()
{
}


void CallUdp::ping()
{
    QByteArray paquet;
    QDataStream out(&paquet, QIODevice::WriteOnly);


    out << (quint64)0;
    out << (quint8)PING;
    out.device()->seek(0);
    out << (quint64)paquet.size()-sizeof(quint64);
    qDebug() << "Ping: " << sockOut->writeDatagram(paquet,
                                                      QHostAddress(ip), port);
}

void CallUdp::reponse()
{

    QDataStream in(sockIn);

    quint8 rep;
    in >> rep;
    qDebug()<< "reponse of share: " <<QString::number(rep);
    if (rep == PING)
    {
        timerOut.start(10000);
    }
    else if (rep == STOPSOUND)
    {
        hangUp();
    }
    else if (rep == NEWSOUND)
    {
        playSound(in);
    }
    else
        return;
}

void CallUdp::playSound(QDataStream &in)
{

}

void CallUdp::stopCall()
{
    QByteArray paquet;
    QDataStream out(&paquet, QIODevice::WriteOnly);


    out << (quint64)0;
    out << (quint8)STOPSOUND;
    out.device()->seek(0);
    out << (quint64)paquet.size()-sizeof(quint64);
    qDebug() << "Write: " << sockOut->writeDatagram(paquet,
                                                      QHostAddress(ip), port);
}

void CallUdp::startCall()
{

        sendFram();
}

void CallUdp::sendFram()
{
  //  int messageNo = 1;
//    QByteArray datagram = "Broadcast message " + QByteArray::number(messageNo);

    QByteArray paquet;
    QDataStream out(&paquet, QIODevice::WriteOnly);
    out << (quint64)0;
    out << (quint8)NEWSOUND;

    out.device()->seek(0);
    out << (quint64)paquet.size()-sizeof(quint64);
    qDebug()<<"paquet size: "<<QString::number(paquet.size()-sizeof(quint32));
//    qDebug()<<"paquet write: "<<clientConnection->writeDatagram(paquet, QHostAddress("192.168.1.8"), 4545);

    qDebug() << "Write: " << sockOut->writeDatagram(paquet,
                                                      QHostAddress(ip), port);
}
