#ifndef CONNEXION_H
#define CONNEXION_H

#include <QWidget>
#include <QtGui>

class Connexion : public QWidget
{
    Q_OBJECT
public:
    explicit Connexion(QWidget *parent = 0);
    
signals:
    void connection(QString, QString);
    void newAccount();

public slots:
    void clean()
    {
        logL->setText("");
        pwdL->setText("");
    }

    void ok_Clicked()
    {
        if (ret->isChecked())
        {
            QSettings settings("Epitech", "Babel 2012");
            settings.beginGroup("Connection");
            settings.setValue("log", logL->text());
            settings.setValue("pwd", pwdL->text());
            settings.endGroup();
        }
        connection(logL->text(), pwdL->text());
    }

    void cree_Clicked()
    {
        newAccount();
    }

    void refreshBut()
    {
        if (logL->text() != "" && pwdL->text() != "")
            ok->setEnabled(true);
        else
            ok->setEnabled(false);
    }

private:
    QLabel * titre;
    QLabel *log;
    QLineEdit *logL;
    QLabel *pwd;
    QLineEdit *pwdL;
    QCheckBox *ret;
    QLabel *retL;
    QPushButton *ok;
    QPushButton *cree;
};

#endif // CONNEXION_H
