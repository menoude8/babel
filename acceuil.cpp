#include "acceuil.h"


Acceuil::Acceuil(QWidget *parent) :
    QWidget(parent)
{

    scene =  new QGraphicsScene();
    scene->setSceneRect(0, 0, 1000, 600);



    title = new QLabel("Acceuil", this);
    QVBoxLayout * layout = new QVBoxLayout(this);
    layout->addWidget(title);

    setLayout(layout);
    widgets = XMLPlugin::lireWidget();
    std::map<IPluginWidget *, WidgetAttr>::iterator it = widgets.begin();

    int i = 0;
    for (;it != widgets.end(); ++it)
    {
       ProxyWidget *proxy = new ProxyWidget();

       IPluginWidget * wd = (*it).first;
       widgetProxy[wd] = proxy;
       QWidget *itemW = wd->getWidget();
//       qDebug() << "WIDGET: " << (*it).first << " adress: " << wd->getWidget();;
       proxy->setWidget(itemW);
       scene->addItem(proxy);
        WidgetAttr pos = (*it).second;
        proxy->setPositionX(pos.x);
        proxy->setPositionY(pos.y);
        QTransform matrix;
       matrix.translate(pos.x, pos.y);
       matrix.rotate(pos.rx, Qt::XAxis);
       matrix.rotate(pos.ry, Qt::YAxis);
       matrix.scale(pos.sx, pos.sy);
       proxy->setTransform(matrix);
       i++;

    }

     view = new QGraphicsView(scene);
     layout->addWidget(view);
}


