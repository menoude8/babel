#ifndef HEADERCHAT_H
#define HEADERCHAT_H

#include <QtGui>
#include "Protocole.h"

class HeaderChat : public QGroupBox
{
    Q_OBJECT
public:
    explicit HeaderChat(User *, QWidget *parent = 0);
    QPixmap resizePic(QPixmap pix, int wmax, int hmax)
    {
        QSize size = pix.size();
        int w = size.width();
        int h = size.height();
        if (h > hmax)
        {
            h = hmax;
            w = size.width() *h/ size.height();
        }
        if (w > wmax)
        {
            w = wmax;
            h = size.height() *w/ size.width();
        }
        return pix.scaled(w, h);

    }
    void setUser(User st)
    {

        rang->setText(st.rang);
        if (st.avatar.isNull())
            pix->setPixmap(resizePic(QPixmap(":/assets/default-avatar.png"),80,80));
        else
            pix->setPixmap(resizePic(QPixmap::fromImage(st.avatar), 80, 80));
        switch (st.etat)
            {
            case OffLine:
                statePic->setPixmap(resizePic(QPixmap(":/assets/offline.png"), 20, 20));
                state->setText("Deconnecte");
            break;

            case OnLine:
                statePic->setPixmap(resizePic(QPixmap(":/assets/online.png"), 20, 20));
                state->setText("Connecte");
            break;
            case Away:

                statePic->setPixmap(resizePic(QPixmap(":/assets/away.png"), 20, 20));
                state->setText("Absent");

                break;
            case Busy:

                statePic->setPixmap(resizePic(QPixmap(":/assets/busy.png"), 20, 20));
                state->setText("Occupe");

                break;
            default:
                statePic->setPixmap(resizePic(QPixmap(":/assets/offline.png"), 20, 20));
                state->setText("Deconnecte");
                break;
        }
    }

signals:
    void Close();

public slots:

private:
    User *user;
    QLabel *pix;
    QLabel *rang;
    QLabel *rangPic;
    QLabel *log;
    QLabel *statePic;
    QLabel *state;
    QLabel *loc;
    QPushButton *close;
};

#endif // HEADERCHAT_H
