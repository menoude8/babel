#include "addrss.h"

addRSS::addRSS(QWidget *parent) :
    QWidget(parent)
{
    setObjectName("addRSS");
    setWindowTitle("add RSS");
    setWindowIcon(QIcon(":/assets/icon.png"));
    layout = new QGridLayout(this);
    cancel = new QPushButton("cancel", this);
    connect(cancel, SIGNAL(clicked()), this, SLOT(close()));
    add = new QPushButton("add", this);
    add->setEnabled(false);
    connect(add, SIGNAL(clicked()), this, SLOT(send()));
    name = new QLineEdit( this);
    url = new QLineEdit( this);
    QLabel *lName = new QLabel("name", this);
    QLabel *lUrl = new QLabel("url", this);

    connect(name, SIGNAL(textChanged(QString)), this, SLOT(Enable()));
    connect(url, SIGNAL(textChanged(QString)), this, SLOT(Enable()));
    layout->addWidget(lUrl, 0, 0, 1, 2);
    layout->addWidget(url, 1, 0, 1, 2);
    layout->addWidget(lName, 2, 0, 1, 2);
    layout->addWidget(name, 3, 0, 1, 2);
    layout->addWidget(add, 4, 0, 1, 1);
    layout->addWidget(cancel, 4, 1, 1, 1);
    setLayout(layout);

}

void addRSS::send()
{
    addFlux(name->text(), url->text());
    name->setText("");
    url->setText("");
    close();
}

void addRSS::Enable()
{
    if (!url->text().isEmpty() && !name->text().isEmpty())
        add->setEnabled(true);
    else
        add->setEnabled(false);
}
