#ifndef CONTACTITEM_H
#define CONTACTITEM_H

#include <QtGui>
#include "Protocole.h"

class ContactItem : public QWidget
{
    Q_OBJECT
public:
    explicit ContactItem(User *u, QWidget *parent = 0);
    QPixmap resizePic(QPixmap pix, int wmax, int hmax)
    {
        QSize size = pix.size();
        int w = size.width();
        int h = size.height();
        if (h > hmax)
        {
            h = hmax;
            w = size.width() *h/ size.height();
        }
        if (w > wmax)
        {
            w = wmax;
            h = size.height() *w/ size.width();
        }
        return pix.scaled(w, h);

    }

    void setUser(User *u)
    {

        user = u;
        if (u->avatar.isNull())
            avatar->setPixmap(resizePic(QPixmap(":/assets/default-avatar.png"), 40, 40));
        else
            avatar->setPixmap(resizePic(QPixmap::fromImage(u->avatar), 40, 40));
        switch (u->etat)
        {
        case OffLine:
            state->setPixmap(resizePic(QPixmap(":/assets/offline.png"), 20,20));
        break;
        case OnLine:
            state->setPixmap(resizePic(QPixmap(":/assets/online.png"), 20,20));
        break;
        case Busy:
            state->setPixmap(resizePic(QPixmap(":/assets/busy.png"), 20,20));
        break;
        case Away:
            state->setPixmap(resizePic(QPixmap(":/assets/away.png"), 20,20));
        break;
        }
        login->setText(u->log);
        rang->setText(u->rang);


    }
    User *getUser() { return user;}
    void addNewMessage() {newMessage->setPixmap(resizePic(QPixmap(":/assets/newmessage.png"), 20, 20));}
    void delNewMessage() {newMessage->setPixmap(resizePic(QPixmap(), 20, 20));}
    void addNewCall() {apelManque->setPixmap(resizePic(QPixmap(":/assets/callmissed.png"), 20, 20));}
    void delNewCall() {apelManque->setPixmap(resizePic(QPixmap(), 20, 20));}

signals:
    void callSignal(User *);
    void messageSignal(User *);
    void screenSignal(User *);

public slots:

    void callClick()
    {
        callSignal(user);
    }
    void messageClick()
    {
        messageSignal(user);
    }
    void screenClick()
    {
        screenSignal(user);
    }

    void ShowContextMenu(const QPoint& pos) // this is a slot
    {
         QPoint globalPos = this->mapToGlobal(pos);

        QMenu myMenu;
        connect(myMenu.addAction("Appeler"), SIGNAL(triggered()), this, SLOT(callClick()));
        connect(myMenu.addAction("Message"), SIGNAL(triggered()), this, SLOT(messageClick()));
        connect(myMenu.addAction("Partage d'ecran"), SIGNAL(triggered()), this, SLOT(screenClick()));


        QAction* selectedItem = myMenu.exec(globalPos);
        if (selectedItem)
        {
            // something was chosen, do stuff
        }
        else
        {
            // nothing was chosen
        }
    }

private:
    User *user;
    QLabel *avatar;
    QLabel *state;
    QLabel *login;
    QLabel *rang;
    QLabel *newMessage;
    QLabel *apelManque;



};

#endif // CONTACTITEM_H
