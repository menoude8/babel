#include "callbar.h"

CallBar::CallBar(QWidget *parent) :
    QGroupBox(parent)
{
    setObjectName("CallsBar");

    QHBoxLayout *layout = new QHBoxLayout(this);
    share = new QPushButton(QIcon(":/assets/share.png"), "", this);
    mute = new QPushButton(QIcon(":/assets/mute.png"), "", this);
    option = new QPushButton(QIcon(":/assets/option.png"), "", this);
    hangUp = new QPushButton(QIcon(":/assets/hangup.png"), "", this);
    connect(option, SIGNAL(clicked()), this, SIGNAL(optionClick()));
    connect(hangUp, SIGNAL(clicked()), this, SIGNAL(hangUpSignal()));
    connect(mute, SIGNAL(clicked()), this, SLOT(muteClick()));
    connect(share, SIGNAL(clicked()), this, SLOT(shareClick()));


    share->setObjectName("CallBarBut");
    mute->setObjectName("CallBarBut");
    option->setObjectName("CallBarBut");
    hangUp->setObjectName("CallBarBut");

    layout->addWidget(share);
    layout->addWidget(mute);
    layout->addWidget(option);
    layout->addWidget(hangUp);
    setLayout(layout);
    isSharing = false;
    isMute = false;
}
