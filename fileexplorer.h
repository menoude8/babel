#ifndef FILEEXPLORER_H
#define FILEEXPLORER_H

#include <QWidget>
#include <QtGui>

class fileExplorer : public QWidget
{
    Q_OBJECT
public:
    explicit fileExplorer(QWidget *parent = 0);

    QString  getPath()
    {
        QModelIndex index = table->currentIndex();
        return model->filePath(index);
    }

signals:
    void fileSelected(QString path);

public slots:
    void goToTree(QModelIndex);
    void goToList(QModelIndex);
    void goTo();
    void GoToParent();
    void sendPath();
    void upButton(QModelIndex);

private:

    QLineEdit *path;
    QListView   *table;
     QTreeView   *arbre;
    QDirModel   *model;
      QDirModel   *modelDir;
    QDir        *CurPath;
    QLineEdit   *name;
    QPushButton *back;
    QPushButton *enter;
    QPushButton *cancel;
    QGridLayout *lay;

};

#endif // FILEEXPLORER_H
