#ifndef FILERECEIVER_H
#define FILERECEIVER_H

#include <QWidget>
#include <QtGui>
#include <QTcpSocket>

#include "fileexplorer.h"
#include "protocole.h"
#include "receiverthread.h"

class fileReceiver : public QWidget
{
    Q_OBJECT
public:
    fileReceiver(QString i, quint16 p, QString n, QString log, QWidget *parent = 0);
    ~fileReceiver();

private:
    receiverThread  *th;
    fileExplorer    *explorer;
    quint16         port;
    QStackedWidget  *stack;
    QGridLayout     *layout1;
    QGridLayout     *layout2;
    QGridLayout     *mainLayout;
    QPushButton     *accept;
    QPushButton     *cancel;
    QPushButton     *refuse;
    QProgressBar    *bar;
    QTcpSocket      *sock;
    quint64         size;
    QString         name;
    QLabel          *label1;
    QLabel          *label2;
    QString ip;

signals:
    void end();

private slots:
    void receptionOK();
    void acceptFile();
    void refuseFile();
    void donneesRecues();
    void pathSelected();
    void changeValue(int);

};

#endif // FILERECEIVER_H
