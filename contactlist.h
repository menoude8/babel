#ifndef CONTACTLIST_H
#define CONTACTLIST_H

#include <QWidget>
#include <QtGui>
#include <map>
#include "Protocole.h"
#include "contactelem.h"
#include "addcontact.h"
#include "contactitem.h"
#include "Popup.h"

class ContactList : public QWidget
{
    Q_OBJECT
public:
    explicit ContactList(User *u, QWidget *parent = 0);
    User *getContact(QString l) { return contact[l]; }
    QPixmap resizePic(QPixmap pix, int wmax, int hmax)
    {
        QSize size = pix.size();
        int w = size.width();
        int h = size.height();
        if (h > hmax)
        {
            h = hmax;
            w = size.width() *h/ size.height();
        }
        if (w > wmax)
        {
            w = wmax;
            h = size.height() *w/ size.width();
        }
        return pix.scaled(w, h);

    }

signals:
    void callSignal(User *);
    void screenSignal(User *);

    void statusChanged(int);
    void rangChanged(QString);
    void addChat(User *);
    void inviteContact(QString);
public slots:
    void refreshState(QString log)
    {
        QIcon icon;
        if (contactListItem[log] == NULL)
        {
            contactListItem.erase(log);
            return;
        }

        contactListItem[log]->setUser(contact[log]);
  }
    
    void addContact(User *c)
    {
        qDebug() << "add : " + c->log;
        QString log = c->log;
        contact[log] = c;
        ContactItem *itemWidget = new ContactItem(c, this);
        connect(itemWidget, SIGNAL(callSignal(User *)), this, SIGNAL(callSignal(User *)));
        connect(itemWidget, SIGNAL(screenSignal(User *)), this, SIGNAL(screenSignal(User *)));
        connect(itemWidget, SIGNAL(messageSignal(User *)), this, SIGNAL(addChat(User*)));

        QListWidgetItem *item = new QListWidgetItem();
/*        switch (c->etat)
        {
        case OffLine:
            item->setIcon(QIcon(":/assets/offline.png"));
        break;
        case OnLine:
            item->setIcon(QIcon(":/assets/online.png"));
        break;
        case Busy:
            item->setIcon(QIcon(":/assets/busy.png"));
        break;
        case Away:
            item->setIcon(QIcon(":/assets/away.png"));
        break;
        }

            item->setText(c->log + " - " + c->rang);
  */
       contactListItem[log] = itemWidget;
       item->setSizeHint(itemWidget->sizeHint());

        liste->addItem(item);
        liste->setItemWidget(item, itemWidget);
    }

    void refreshUser()
    {
        switch (user->etat)
        {
        case OffLine:
        etat->setPixmap(resizePic(QPixmap(":/assets/offline.png"), 50, 50));
        break;
        case OnLine:
        etat->setPixmap(resizePic(QPixmap(":/assets/online.png"),50, 50));
        break;
        case Away:
        etat->setPixmap(resizePic(QPixmap(":/assets/away.png"),50, 50));
        break;
        case Busy:
        etat->setPixmap(resizePic(QPixmap(":/assets/busy.png"),50, 50));
        break;

        }
        if (user->rang.length() > 20)
        {
            QString tmp = user->rang;
            tmp.truncate(17);
            tmp += " ...";
            rang->setText(tmp);

        }
        else
            rang->setText(user->rang);
        avatar->setPixmap(QPixmap::fromImage(user->avatar));
    }

    void modifyContact(User *c)
    {
        if (contact[c->log] == NULL)
        {
            addContact(c);
        }
        else
        {
            if (contact[c->log]->etat ==  OffLine && c->etat != OffLine)
            {
                Popup *t =  new Popup(200,140);
                t->Show("Nouvelle connection", c->log + " vient de ce connecter.", 5000);
                QSound::play("son/connect.wav");
            }
            contact[c->log] = c;
            refreshState(c->log);
        }
    }
    void refreshStatus(int st)
    {
        user->etat = (State)st;
        switch (user->etat)
        {
        case OffLine:
        etat->setPixmap(resizePic(QPixmap(":/assets/offline.png"), 50, 50));
        break;
        case OnLine:
        etat->setPixmap(resizePic(QPixmap(":/assets/online.png"),50, 50));
        break;
        case Away:
        etat->setPixmap(resizePic(QPixmap(":/assets/away.png"),50, 50));
        break;
        case Busy:
        etat->setPixmap(resizePic(QPixmap(":/assets/busy.png"),50, 50));
        break;

        }
    }
    void newChat(QModelIndex id)
    {
        QString log = ((ContactItem *)liste->itemWidget(liste->currentItem()))->getUser()->log;
        addChat(contact[log]);
    }
    void addClick()
    {
        if (newCo)
            delete newCo;
        newCo = new AddContact();
        newCo->show();
        connect(newCo, SIGNAL(Add(QString)), this, SLOT(askForContact(QString)));
        connect(newCo, SIGNAL(Cancel()), this, SLOT(addCancel()));
    }

    void addCancel()
    {
        newCo->close();
    }

    void askForContact(QString log)
    {
        newCo->close();
        inviteContact(log);
    }

    void setRang()
    {
        user->rang = rangL->text();
        rangChanged(user->rang);
        if (user->rang.length() > 20)
        {
            QString tmp = user->rang;
            tmp.truncate(17);
            tmp += " ...";
            rang->setText(tmp);

        }
        else
            rang->setText(user->rang);
        rangL->setText("");
    }

private:
    AddContact *newCo;
    User *user;
    QLabel *etat;
    QLabel *avatar;
    QLabel *login;
    QLabel *rang;
    QLineEdit *rangL;
    QPushButton *rangB;

    QComboBox *status;
    std::map<QString, User *> contact;
    std::map<QString, ContactItem *> contactListItem;
    QListWidget *liste;
    QPushButton *add;
};

#endif // CONTACTLIST_H
