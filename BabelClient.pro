QT += network xml webkit phonon

RC_FILE = mkapp.rc

HEADERS += \
    mainwindow.h \
    acceuil.h \
    imageitem.h \
    fileexplorer.h \
    rsslisting.h \
    addrss.h \
    dialserv.h \
    connexion.h \
    Protocole.h \
    contactlist.h \
    contactelem.h \
    widgetChat.h \
    emoticone.h \
    tabwidget.h \
    addcontact.h \
    playlistwidget.h \
    ftp.h \
    majour.h \
    newaccount.h \
    resultPage.h \
    resultElement.h \
    headerchat.h \
    centralpage.h \
    serialisationout.h \
    serialisationin.h \
    contactitem.h \
    IPluginOperation.h \
    pluginswidget.h \
    pluginitem.h \
    pluginlist.h \
    IPluginWidget.h \
    xmlplugin.h \
    Popup.h \
    proxywidget.h \
    callconfirmation.h \
    sendframe.h \
    screensender.h \
    screenreceiver.h \
    receiverthread.h \
    receiveframe.h \
    filesender.h \
    filereceiver.h \
    callreceiver.h \
    shareconfirmation.h \
    p2pconnection.h \
    callbar.h \
    calloption.h \
    profil.h \
    calludp.h
   # addemoticone.h

SOURCES += \
    mainwindow.cpp \
    main.cpp \
    acceuil.cpp \
    imageitem.cpp \
    fileexplorer.cpp \
    rsslisting.cpp \
    addrss.cpp \
    dialserv.cpp \
    connexion.cpp \
    contactlist.cpp \
    contactelem.cpp \
    widgetChat.cpp \
    emoticone.cpp \
    tabwidget.cpp \
    addcontact.cpp \
    playlistwidget.cpp \
    ftp.cpp \
    majour.cpp \
    newaccount.cpp \
    resultPage.cpp \
    resultElement.cpp \
    headerchat.cpp \
    centralpage.cpp \
    serialisationout.cpp \
    serialisationin.cpp \
    contactitem.cpp \
    pluginswidget.cpp \
    pluginitem.cpp \
    pluginlist.cpp \
    xmlplugin.cpp \
    proxywidget.cpp \
    callconfirmation.cpp \
    sendframe.cpp \
    screensender.cpp \
    screenreceiver.cpp \
    receiverthread.cpp \
    receiveframe.cpp \
    filesender.cpp \
    filereceiver.cpp \
    callreceiver.cpp \
    shareconfirmation.cpp \
    p2pconnection.cpp \
    callbar.cpp \
    calloption.cpp \
    profil.cpp \
    calludp.cpp

RESOURCES += \
    ressource.qrc

OTHER_FILES += \
    ../MKDex-build-desktop-Qt_4_7_4_for_Desktop_-_MinGW_4_4__Qt_SDK__Debug/debug/feuille.css \
    ../MKDex-build-desktop-Qt_4_7_4_for_Desktop_-_MinGW_4_4__Qt_SDK__Debug/debug/pays.bab \
    ../MKDex-build-desktop-Qt_4_7_4_for_Desktop_-_MinGW_4_4__Qt_SDK__Debug/debug/langue.bab

FORMS += \
    calloption.ui






























































