#include "connexion.h"

Connexion::Connexion(QWidget *parent) :
    QWidget(parent)
{
    setMinimumHeight(550);
    QGridLayout * layout = new QGridLayout(this);
    titre = new QLabel("Connection", this);
    QSettings settings("Epitech", "Babel 2012");
    log = new QLabel("User", this);
    logL = new QLineEdit(settings.value("Connection/log").toString(), this);

    pwd = new QLabel("PassWord", this);
    pwdL = new QLineEdit(settings.value("Connection/pwd").toString(), this);
    pwdL->setEchoMode(QLineEdit::Password);

    ret = new QCheckBox(this);
    retL = new QLabel("Retenir", this);
    cree = new QPushButton("Cree un compte", this);
    ok = new QPushButton("Connection", this);
    connect(ok, SIGNAL(clicked()), this, SLOT(ok_Clicked()));
    connect(cree, SIGNAL(clicked()), this, SLOT(cree_Clicked()));

    connect(logL, SIGNAL(textChanged(QString)), this, SLOT(refreshBut()));
    connect(pwdL, SIGNAL(textChanged(QString)), this, SLOT(refreshBut()));
    layout->addWidget(titre, 0, 0, 1, 1, Qt::AlignHCenter | Qt::AlignTop);

    QVBoxLayout *lay = new QVBoxLayout();

    QHBoxLayout *logLay = new QHBoxLayout();
    logLay->addWidget(log, 0, Qt::AlignRight);
    logLay->addWidget(logL, 0,  Qt::AlignLeft);
    lay->addLayout(logLay);

    QHBoxLayout *pwdLay = new QHBoxLayout();
    pwdLay->addWidget(pwd, 0, Qt::AlignRight);
    pwdLay->addWidget(pwdL, 0,  Qt::AlignLeft);
    lay->addLayout(pwdLay);

    QHBoxLayout *retLay = new QHBoxLayout();
    retLay->addWidget(ret, 0, Qt::AlignRight);
    retLay->addWidget(retL, 0,  Qt::AlignLeft);
    lay->addLayout(retLay);


    QHBoxLayout *butLay = new QHBoxLayout();
    butLay->addWidget(cree, 0, Qt::AlignRight);
    butLay->addWidget(ok, 0,  Qt::AlignLeft);
    lay->addLayout(butLay);

    QWidget *wid = new QWidget(this);
    wid->setLayout(lay);
    wid->setFixedHeight(150);

    QWidget *wid2 = new QWidget(this);
     QHBoxLayout *l = new QHBoxLayout();
     l->addWidget(wid);
     wid2->setLayout(l);
     wid2->setMinimumHeight(500);
     layout->addWidget(wid2, 1, 0, 1, 1, Qt::AlignCenter);


    setLayout(layout);

    refreshBut();
}
