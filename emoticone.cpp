#include "emoticone.h"

emoticone::emoticone(QWidget *parent) :
    QDialog(parent, Qt::Popup)
{
    initEmoticone();

    layout = new QGridLayout(this);
    int i = 0;
    j = 0;

    tool = new QToolBar(this);
    layout->addWidget(tool, i, 0, 1, 1);
    for (std::map<QString, QString>::iterator it = emoticones.begin();
         it != emoticones.end(); ++it)
    {
        if (j == 5)
        {
            i++;
            tool = new QToolBar(this);
            layout->addWidget(tool, i, 0, 1, 1);
            j = 0;
        }
        actions[(*it).first] = tool->addAction(QIcon((*it).second), (*it).first);
        connect(actions[(*it).first], SIGNAL(triggered()), this, SLOT(sendEmo()));
        j++;
    }
    setLayout(layout);
}

void emoticone::initEmoticone()
{
    QFile file("conf/emoticone.bab");
    if (!file.open(QIODevice::ReadOnly))
               QMessageBox::information(this, tr("File"),tr("emoticone.bab cannot be opened. See on your conf folder."));
    QString line;
    QString key, path;
    while (!file.atEnd())
    {
        line = file.readLine();
        key = line.section(' ', 0, 0);
        path = line.section(' ', 1, 1);
        path.remove("\n");
        path.remove("\r");
        emoticones[key] = path;
    }
    file.close();
}

QString emoticone::modelise(QString text)
{

    for (std::map<QString, QString>::iterator it = emoticones.begin(); it != emoticones.end(); ++it)
        text.replace((*it).first, "<img src=\"" + (*it).second + "\" />");
    return text;
}

void emoticone::sendEmo()
{
    QAction * emetteur = (QAction *)sender();
    sendEmoticone(emetteur->text());

}
