#ifndef FTP_H
#define FTP_H

#include <QtGui>
#include <QUrlInfo>
#include <QFtp>

class Ftp : public QWidget
{
    Q_OBJECT
public:
    explicit Ftp(QWidget *parent = 0);
    
    QSize sizeHint() const;
signals:
    void newDLFile();
    void actualiseVar(QString);

private slots:
         void connectOrDisconnect();
         void downloadFile();
         void cancelDownload();

         void ftpCommandFinished(int commandId, bool error);
         void addToList(const QUrlInfo &urlInfo);
         void processItem(QTreeWidgetItem *item, int column);
         void cdToParent();
         void updateDataTransferProgress(qint64 readBytes,
                                         qint64 totalBytes);
         void enableDownloadButton();


private:
         QLabel *ftpServerLabel;

         QLabel *statusLabel;
         QTreeWidget *fileList;
         QPushButton *cdToParentButton;
         QPushButton *downloadButton;
         QProgressBar *progressDialog;
         QLabel *progressLabel;
         QHash<QString, bool> isDirectory;
         QString currentPath;
         QFtp *ftp;
         QFile *file;

     #ifdef Q_OS_SYMBIAN
         bool bDefaultIapSet;
     #endif
};

#endif // FTP_H
