#include "centralpage.h"
#include "ftp.h"

centralPage::centralPage(User *u, QWidget *parent) :
    QWidget(parent), user(u)
{
    setObjectName("homePage");
    setMinimumWidth(300);
    acceuil = NULL;
    QVBoxLayout *layout = new QVBoxLayout(this);
    QHBoxLayout *tabLay = new QHBoxLayout(this);
  //  header = new QLabel(this);
    //header->setObjectName("tabLabel");
    homeBut = new QPushButton(QIcon(":/assets/home.png"),"", this);
    homeBut->setObjectName("tabBut");
    homeBut->setEnabled(false);
    homeBut->setFixedWidth(60);
    actuBut = new QPushButton(QIcon(":/assets/shop.png"),"", this);
    actuBut->setFixedWidth(60);
    actuBut->setObjectName("tabBut");
    profilBut = new QPushButton(QIcon(":/assets/profil.png"),"", this);
    profilBut->setFixedWidth(60);
    profilBut->setObjectName("tabBut");

    pluginBut = new QPushButton(QIcon(":/assets/plugin.png"),"", this);
    pluginBut->setFixedWidth(60);
    pluginBut->setObjectName("tabBut");

    connect(homeBut,SIGNAL(clicked()), this, SLOT(homeClick()));
    connect(actuBut,SIGNAL(clicked()), this, SLOT(actuClick()));
    connect(profilBut,SIGNAL(clicked()), this, SLOT(profilClick()));
    connect(pluginBut,SIGNAL(clicked()), this, SLOT(pluginClick()));
    stack = new QStackedWidget(this);
    acceuil =  new Acceuil();
    stack->insertWidget(0, acceuil);
    stack->insertWidget(1, new Ftp());
    stack->insertWidget(2, new QWidget());
    stack->insertWidget(3, new QWidget());
    //tabLay->setSpacing(0);
    //tabLay->setMargin(0);
    //layout->setSpacing(0);
    //layout->setMargin(0);
    QWidget *menu = new QWidget(this);
    tabLay->addWidget(homeBut);
    tabLay->addWidget(profilBut);
    tabLay->addWidget(pluginBut);
    tabLay->addWidget(actuBut);

    menu->setLayout(tabLay);
  //  tabLay->addWidget(header);
    layout->addWidget(menu, 0, Qt::AlignLeft);
//    QVBoxLayout *l = new QVBoxLayout;
  //  l->addWidget(stack);

    layout->addWidget(stack);
    setLayout(layout);
}
