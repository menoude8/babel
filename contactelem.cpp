#include "contactelem.h"

ContactElem::ContactElem(User &c, QWidget *parent) :
    QPushButton(parent), cont(c)
{
    state = new QLabel(this);
    switch (cont.etat)
    {
    case OffLine:
        state->setPixmap(QPixmap(":/assets/offline.png").scaled(30, 30));
    break;
    case OnLine:
        state->setPixmap(QPixmap(":/assets/online.png").scaled(30, 30));
    break;
    case Busy:
        state->setPixmap(QPixmap(":/assets/busy.png").scaled(30, 30));
    break;
    case Away:
        state->setPixmap(QPixmap(":/assets/away.png").scaled(30, 30));
    break;
    }
    qDebug() << "login: " << cont.log;
    login = new QLabel(cont.log, this);
    rang =  new QLabel(this);


        rang->setText(cont.rang);

    QHBoxLayout *lay = new QHBoxLayout(this);
    lay->addWidget(state);
    lay->addWidget(login);
    lay->addWidget(rang);
    setLayout(lay);
}
