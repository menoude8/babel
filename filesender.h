#ifndef FILESENDER_H
#define FILESENDER_H

#include <QtNetwork>
#include <QObject>
#include <QtGui>

#include "fileexplorer.h"
#include "protocole.h"

class fileSender : public QWidget
{
    Q_OBJECT
public:
    fileSender(QString l, QWidget* parent = NULL);
    ~fileSender();
    quint16 getPort() { return sock->serverPort(); }

private slots:
    void newClient();
    void selectFile();
    void fileSelected();
    void sendF();
    void reponse();
    void sendFil();

signals:
    void sendItem( QString, QString, quint16);
    void writeOnScreen(QString);

protected:
    void run();

private:
    fileExplorer *explorer;
    QString login;
    QGridLayout *layout;

    QLineEdit   *Path;
    QPushButton *Browse;
    QPushButton *sendFile;

    QTcpServer *sock;
    QTcpSocket *clientConnection;

};

#endif // FILESENDER_H
