#ifndef CALLBAR_H
#define CALLBAR_H

#include <QtGui>

class CallBar : public QGroupBox
{
    Q_OBJECT
public:
    explicit CallBar(QWidget *parent = 0);
    
signals:
    void optionClick();
    void startShare();
    void stopShare();
    void muteSignal();
    void muteNotSignal();

    void hangUpSignal();
public slots:
    void muteClick()
    {
        if (isMute)
        {
            mute->setIcon(QIcon(":/assets/mute.png"));
            muteNotSignal();
        }
        else
        {
            mute->setIcon(QIcon(":/assets/volume_up.png"));
            muteSignal();
        }
        isMute = !isMute;
    }
    void shareClick()
    {
        if (isSharing)
        {
            share->setIcon(QIcon(":/assets/share.png"));
            stopShare();
        }
        else
        {
            share->setIcon(QIcon(":/assets/sharenot.png"));
            startShare();
        }
        isSharing = !isSharing;

    }
    bool getIsSharing()
    {
        return isSharing;
    }
    void setIsSharing(bool s)
    {
        qDebug() << "SETSHARING";
        isSharing = s;
        if (!isSharing)
        {
            share->setIcon(QIcon(":/assets/share.png"));
        }
        else
        {
            share->setIcon(QIcon(":/assets/sharenot.png"));
        }

    }
    bool getIsMute()
    {
        return isMute;
    }
    void setIsMute(bool s)
    {
        isMute = s;
        if (!isMute)
        {
            mute->setIcon(QIcon(":/assets/mute.png"));
        }
        else
        {
            mute->setIcon(QIcon(":/assets/volume_up.png"));
        }

    }

private:
    bool isMute;
    bool isSharing;
    QPushButton *share;
    QPushButton *mute;
    QPushButton *option;
    QPushButton *hangUp;
};

#endif // CALLBAR_H
