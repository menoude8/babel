#ifndef ADDEMOTICONE_H
#define ADDEMOTICONE_H

#include <QWidget>
#include <QtGui>

#include "fileexplorer.h"

class addEmoticone : public QWidget
{
    Q_OBJECT
public:
    explicit addEmoticone(QWidget *parent = 0);
    ~addEmoticone()
    {
        delete exp;
    }

signals:
    void add(QString, QString);

public slots:
    void send();
    void setPath();
    void Enable();

private:
    fileExplorer *exp;
    QGridLayout  *layout;
    QPushButton  *addButton;
    QPushButton  *browse;
    QPushButton  *cancel;
    QLineEdit    *path;
    QLineEdit    *shortCut;
};

#endif // ADDEMOTICONE_H
