#ifndef DIALSERV_H
#define DIALSERV_H

#include <QWidget>
#include <QtGui>
#include <QtNetwork>
#include "connexion.h"
#include "Protocole.h"
#include "contactlist.h"
#include "newaccount.h"

class DialServ : public QObject
{
    Q_OBJECT
public:
    explicit DialServ(QObject *parent = 0);
    
    void authentification();
    void repForLog();
    void receiveSearch();
    void setState();
    void rcvMsg();
    void rcvPub();
    void askForContact();
    User *getUser() { return user; }
    void newInvite();
    void inviteSended();
    void inviteNotSended();
    void inviteOk();
    void miseAJour();
    void askForCall();
    void repForCall();
    void askForShare();
    void repForShare();

    int chargeQString(QByteArray *paquet, QString l)
    {
        int size = l.length();
        paquet->append(((char *)(&size)), sizeof(int));
         paquet->append(l.toAscii().data(), size);
         qDebug() << "CHARGE STRING : " <<l<< " " << size << " octets.";
         return (size + sizeof(int));
    }
    QString dechargeString()
    {
        int size;
        QString ret;

        socket->read((char *)(&size), sizeof(int));
        char *tmp = (char *)malloc(size + 1);

        memset(tmp, 0, size + 1);

        socket->read(tmp, size);
        ret = QString(tmp);
        qDebug() << "DECHARGE STRING : " << ret << "" << size << " octets.";
        free(tmp);
        return ret;
    }

    int chargeInt(QByteArray *paquet, int value)
    {
        paquet->append(((char *)(&value)), sizeof(int));
        return sizeof(int);
    }

    int dechargeInt()
    {
        int size;
        socket->read((char *)(&size), sizeof(int));

        qDebug() << "DECHARGE int: " << size;
        return size;
    }
    int chargeQuint8(QByteArray *paquet, quint8 value)
    {
        paquet->append(((char *)(&value)), sizeof(quint8));

        return sizeof(quint8);
    }

    quint8 dechargeQuint8()
    {
        quint8 size;
        socket->read((char *)(&size), sizeof(quint8));
        qDebug() << "DECHARGE QUINT8 : " << QString::number(size) ;
        return size;
    }
    int chargeQuint64(QByteArray *paquet, quint64 value)
    {
        paquet->append(((char *)(&value)), sizeof(quint64));
        return sizeof(quint64);
    }

    quint64 dechargeQuint64()
    {
        quint64 size;
        socket->read((char *)(&size), sizeof(quint64));
        return size;
    }

    int chargeQuint16(QByteArray *paquet, quint16 value)
    {
        paquet->append(((char *)(&value)), sizeof(quint16));
        return sizeof(quint16);
    }

    quint16 dechargeQuint16()
    {
        quint16 size;
        socket->read((char *)(&size), sizeof(quint16));
        qDebug() << "DECHARGE QUINT16: " << size;
        return size;
    }
    int chargeQPixmap(QByteArray *paquet, QImage value)
    {

       int ret = 0;
       ret += chargeInt(paquet, value.height());
       ret += chargeInt(paquet, value.width());
       ret += chargeQuint8(paquet, value.format());
       ret += chargeInt(paquet, value.numBytes());

       ret += value.numBytes();
       paquet->append((char *)value.bits(),  value.numBytes());
       qDebug() << "CHARGE PIXMAP- height: " << value.height()
                << " - width: " << value.width()
                << " - numbytes: " << value.numBytes()
                << " - format: " << value.format();
       return ret;
    }

    QImage dechargeQPixmap()
    {
       QImage pic;
       int h = dechargeInt();
       int w = dechargeInt();
       quint8 f = dechargeQuint8();
       int size = dechargeInt();

       char *tmp = (char *)malloc(size);
       socket->read(tmp, size);
       pic = QImage((uchar *)tmp, w, h, (QImage::Format)f);
       qDebug() << "DECHARGE PIXMAP- height: " << h
                << " - width: " << w
                << " - numbytes: " << size
                << " - format: " << f;
       return pic;
   }


    int chargeUser(QByteArray *paquet, User *u)
    {
        int ret = 0;

        ret += chargeQuint8(paquet, (quint8)u->etat);
        ret += chargeQString(paquet, u->log);
        ret += chargeQString(paquet, u->rang);
        ret += chargeInt(paquet, u->BDay);
        ret += chargeInt(paquet, u->BMonth);
        ret += chargeInt(paquet, u->BYear);
        ret += chargeQString(paquet, u->city);
        ret += chargeQString(paquet, u->email);
        ret += chargeQString(paquet, u->firstName);
        ret += chargeQString(paquet, u->langue);
        ret += chargeQString(paquet, u->lastName);
        ret += chargeQString(paquet, u->Pays);
        ret += chargeQString(paquet, u->phone);
        ret += chargeQString(paquet, u->pwd);
        ret += chargeQuint8(paquet, (quint8)u->sexe);
        ret += chargeQuint8(paquet, (quint8)u->userType);
        ret += chargeQPixmap(paquet, u->avatar);

        return ret;
    }

    User *dechargerUser()
    {
        User *u = new User();


        u->etat = (State)dechargeQuint8();
        u->log = dechargeString();
        u->rang = dechargeString();
        u->BDay = dechargeInt();
        u->BMonth = dechargeInt();
        u->BYear = dechargeInt();
        u->city = dechargeString();
        u->email = dechargeString();
        u->firstName = dechargeString();
        u->langue = dechargeString();
        u->lastName = dechargeString();
        u->Pays = dechargeString();
        u->phone = dechargeString();
        u->pwd = dechargeString();
        u->sexe = (Sexe)dechargeQuint8();
        u->userType = (UserType)dechargeQuint8();
        u->avatar = dechargeQPixmap();
        return u;
    }
    User *dechargerFriend()
    {
        User *u = new User();


        u->etat = (State)dechargeQuint8();
        u->log = dechargeString();
        u->rang = dechargeString();
        u->BDay = dechargeInt();
        u->BMonth = dechargeInt();
        u->BYear = dechargeInt();
        u->city = dechargeString();
        u->email = dechargeString();
        u->firstName = dechargeString();
        u->langue = dechargeString();
        u->lastName = dechargeString();
        u->Pays = dechargeString();
        u->phone = dechargeString();
        u->sexe = (Sexe)dechargeQuint8();
        u->userType = (UserType)dechargeQuint8();
        u->avatar = dechargeQPixmap();
        return u;
    }


signals:
    void newCall(QString log, QString ip, quint16 port);

    void repCall(QString log, QString ip, quint16 port, bool);
    void newShare(QString, QString ip, quint16 port);
    void repShare(QString, QString ip, quint16 port, bool);
    void connectedToServ(bool);
    void showText(QString);
    void addContact(User *u);
    void modifyContact(User *u);
    void repForLoging(bool);
    void connected(bool);
    void addChat(User *c);
    void newMsg(QString, QString);
    void stateChanged(User &c);
    void searchResult(std::vector<User *> r);
public slots:
    void askForCallSlot(QString);
    void repForCallSlot(QString, bool);
    void askForShareSlot(QString);
    void repForShareSlot(QString, bool);
    void startConnection();
    void addUser(User);
    void search(QString l);
    void askForLog(QString);
    void donneesRecues();
    void connecte();
    void deconnecte();
    void erreurSocket(QAbstractSocket::SocketError erreur);
    void login(QString l, QString p);

    void changeStatus(int st);
    void changeRang(QString rg);
    void changeUser();
    void sendMsg(QString, QString);
    void publicate();
    void inviteContact(QString);

private:
    QTimer *t_connect;
    User *user;
    QTcpSocket *socket;
    quint64 tailleMessage;
};

#endif // DIALSERV_H
