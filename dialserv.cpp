#include "dialserv.h"
#include "majour.h"

DialServ::DialServ(QObject *parent) :
    QObject(parent)
{

    socket = new QTcpSocket(this);
       connect(socket, SIGNAL(readyRead()), this, SLOT(donneesRecues()));
       connect(socket, SIGNAL(connected()), this, SLOT(connecte()));
       connect(socket, SIGNAL(disconnected()), this, SLOT(deconnecte()));
       connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(erreurSocket(QAbstractSocket::SocketError)));
       t_connect = new QTimer(this);
       tailleMessage = 0;
       user = new User();
       user->auth = false;
       startConnection();

}

void DialServ::startConnection()
{
    showText("Connection au server ...");
     t_connect->stop();
    socket->abort();
    QFile f(QCoreApplication::applicationDirPath()+"/address.bab");
    QString ip = "127.0.0.1";
    quint16 port = 50585;
    if(f.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        ip = f.readLine();
        port = ((QString)f.readLine()).toUInt();
        qDebug() << "IP : " << ip;
        qDebug() << "Port: " << port;

        f.close();
    }
    socket->connectToHost(ip, port);
}

/*
// Envoi d'un message au serveur
void DialServ::on_boutonEnvoyer_clicked()
{
    QByteArray paquet;
    QDataStream out(&paquet, QIODevice::WriteOnly);

    // On pr�pare le paquet � envoyer
    QString messageAEnvoyer = tr("<strong>") + pseudo->text() +tr("</strong> : ") + message->text();

    out << (quint64) 0;
    out << messageAEnvoyer;
    out.device()->seek(0);
    out << (quint64) (paquet.size() - sizeof(quint64));

    socket->write(paquet); // On envoie le paquet

    message->clear(); // On vide la zone d'�criture du message
    message->setFocus(); // Et on remet le curseur � l'int�rieur
}

// Appuyer sur la touche Entr�e a le m�me effet que cliquer sur le bouton "Envoyer"
void DialServ::on_message_returnPressed()
{
    on_boutonEnvoyer_clicked();
}

// On a re�u un paquet (ou un sous-paquet)
*/
void DialServ::donneesRecues()
{
    qDebug() << "Size available: " << socket->bytesAvailable();

    if (tailleMessage == 0)
    {
        if (socket->bytesAvailable() < (int)sizeof(quint64))
             return;

        socket->read((char *)(&tailleMessage), sizeof(quint64));
    }
    qDebug() << "Size: " << tailleMessage;
    qDebug() << "Size available: " << socket->bytesAvailable();

    if (socket->bytesAvailable() < tailleMessage)
        return;

    quint8 proto;
    socket->read((char *)(&proto), sizeof(quint8));
    qDebug() << "Receive proto: " << proto;
    switch (proto)
    {
    case AUTH:
        authentification();
        break;
    case ASKFORLOG:
        repForLog();
        break;
    case SETSTATE:
        setState();
        break;
    case SEARCHCONTACT:
        receiveSearch();
        break;
    case MESSAGE:
        rcvMsg();
        break;
    case PUBLICATION:
        rcvPub();
        break;
    case INVITECONTACT:
        newInvite();
        break;
    case INVITESENDED:
        inviteSended();
        break;
    case INVITENOTSENDED:
        inviteNotSended();
        break;
    case INVITEOK:
        inviteOk();
        break;
    case MAJ:
        miseAJour();
        break;
    case ASKFORCALL:
        askForCall();
        break;
    case REPFORCALL:
        repForCall();
        break;
    case ASKFORSHARE:
        askForShare();
        break;
    case REPFORSHARE:
        repForShare();
        break;

    }

   tailleMessage = 0;
   if (socket->bytesAvailable() > 0)
       donneesRecues();

}

void DialServ::connecte()
{
    connectedToServ(true);
    showText("Connexion �tablie");

}

void DialServ::deconnecte()
{
    connected(false);
    showText("D�connect� du serveur");

}

void DialServ::erreurSocket(QAbstractSocket::SocketError erreur)
{
    switch(erreur)
    {
        case QAbstractSocket::HostNotFoundError:
            showText("ERREUR : le serveur n'a pas pu �tre trouv�.");
            break;
        case QAbstractSocket::ConnectionRefusedError:
        showText("ERREUR : le serveur a refus� la connexion.");
            break;
        case QAbstractSocket::RemoteHostClosedError:
        showText("ERREUR : le serveur a coup� la connexion.");
            break;
        default:
        showText("ERREUR : " + socket->errorString());
    }
    t_connect->start(10000);
    connect(t_connect, SIGNAL(timeout()), this, SLOT(startConnection()));
    connectedToServ(false);
    connected(false);
}

void DialServ::askForCallSlot(QString l)
{
    QByteArray paquet;
    QByteArray paquetSize;
    QString ip = socket->localAddress().toString();
    quint16 port = PORTCALL;
    qDebug() << "IP: " << ip << " PORT: " << port;
    quint8 proto = ASKFORCALL;
    paquet.append(((char *)(&proto)), sizeof(quint8));
    chargeQString(&paquet, l);
    chargeQString(&paquet, ip);
    chargeQuint16(&paquet, port);
    quint64 size = (quint64) paquet.size();
    paquetSize.append(((char *)(&size)), sizeof(quint64));
    socket->write(paquetSize);
    socket->write(paquet);
}

void DialServ::repForCallSlot(QString l, bool rep)
{
    QByteArray paquet;
    QByteArray paquetSize;
    QString ip = socket->localAddress().toString();
    quint16 port = PORTCALL;
    qDebug() << "IP: " << ip << " PORT: " << port;

    quint8 proto = REPFORCALL;
    paquet.append(((char *)(&proto)), sizeof(quint8));
    chargeQString(&paquet, l);
    chargeQString(&paquet, ip);
    chargeQuint16(&paquet, port);
    if (rep)
        proto = OK;
    else
        proto = KO;
    paquet.append(((char *)(&proto)), sizeof(quint8));
    quint64 size = (quint64) paquet.size();
    paquetSize.append(((char *)(&size)), sizeof(quint64));
    socket->write(paquetSize);
    socket->write(paquet);
}

void DialServ::askForShareSlot(QString l)
{
    qDebug()<< "Send Asking";
    QByteArray paquet;
    QByteArray paquetSize;
    QString ip = socket->localAddress().toString();
    quint16 port = PORTSHARE;
    qDebug() << "IP: " << ip << " PORT: " << port;

    quint8 proto = ASKFORSHARE;
    paquet.append(((char *)(&proto)), sizeof(quint8));
    chargeQString(&paquet, l);
    chargeQString(&paquet, ip);
    chargeQuint16(&paquet, port);
    quint64 size = (quint64) paquet.size();
    paquetSize.append(((char *)(&size)), sizeof(quint64));
    socket->write(paquetSize);
    socket->write(paquet);
}

void DialServ::repForShareSlot(QString l, bool rep)
{
    QByteArray paquet;
    QByteArray paquetSize;
    QString ip = socket->localAddress().toString();
    quint16 port = PORTSHARE;
    qDebug() << "IP: " << ip << " PORT: " << port;


    quint8 proto = REPFORSHARE;
    paquet.append(((char *)(&proto)), sizeof(quint8));
    chargeQString(&paquet, l);
    chargeQString(&paquet, ip);
    chargeQuint16(&paquet, port);
    if (rep)
        proto = OK;
    else
        proto = KO;
    paquet.append(((char *)(&proto)), sizeof(quint8));
    quint64 size = (quint64) paquet.size();
    paquetSize.append(((char *)(&size)), sizeof(quint64));
    socket->write(paquetSize);
    socket->write(paquet);
}

void DialServ::login(QString l, QString p)
{
    QByteArray paquet;
    QByteArray paquetSize;

    quint8 proto = AUTH;
    paquet.append(((char *)(&proto)), sizeof(quint8));
    chargeQString(&paquet, l);
    chargeQString(&paquet, p);

    quint64 size = (quint64) paquet.size();
    paquetSize.append(((char *)(&size)), sizeof(quint64));
    socket->write(paquetSize);
    socket->write(paquet);

connectedToServ(false);
}

void DialServ::addUser(User cl)
{
    QByteArray paquet;
    QByteArray paquetSize;
    quint8 proto = ADDUSER;
    paquet.append(((char *)(&proto)), sizeof(quint8));
    chargeUser(&paquet, &cl);
    quint64 size = (quint64) paquet.size();
    paquetSize.append(((char *)(&size)), sizeof(quint64));
    socket->write(paquetSize);
    socket->write(paquet); // On envoie le paquet
    connectedToServ(false);
}

void DialServ::askForLog(QString l)
{
    QByteArray paquet;
    QByteArray paquetSize;

    quint8 proto = ASKFORLOG;
    paquet.append(((char *)(&proto)), sizeof(quint8));
    chargeQString(&paquet, l);

    quint64 size = (quint64) paquet.size();
    paquetSize.append(((char *)(&size)), sizeof(quint64));
    socket->write(paquetSize);
    socket->write(paquet);


}

void DialServ::askForCall()
{
    QString log = dechargeString();
    QString ip = dechargeString();
    quint16 port = dechargeQuint16();
    newCall(log, ip, port);
}

void DialServ::repForCall()
{
    QString log = dechargeString();
    QString ip = dechargeString();
    quint16 port = dechargeQuint16();
    quint8 rep = dechargeQuint8();
    if (rep == OK)
        repCall(log, ip, port, true);
    else
        repCall(log, ip, port, false);
}

void DialServ::askForShare()
{
    QString log = dechargeString();
    QString ip = dechargeString();
    quint16 port = dechargeQuint16();
    qDebug() << "askForShare " << log << " IP: " << ip << " PORT: " << port;
    newShare(log, ip, port);
}

void DialServ::repForShare()
{
    QString log = dechargeString();
    QString ip = dechargeString();
    quint16 port = dechargeQuint16();
    quint8 rep = dechargeQuint8();
    if (rep == OK)
        repShare(log, ip, port, true);
    else
        repShare(log, ip, port, false);
}

void DialServ::receiveSearch()
{
   int nb;
  socket->read((char *)(&nb), sizeof(int));
   qDebug() << "Resultat de recherche:";
   std::vector<User *> result;
   for (int i = 0; i < nb; i++)
   {
       User *tmp = dechargerFriend();
       result.push_back(tmp);
       qDebug() << "- " << tmp->log;
   }
   searchResult(result);
}

void DialServ::repForLog()
{
    bool rep;
    quint8 t = dechargeQuint8();
    if (t == OK)
        rep = true;
    else
        rep = false;
    repForLoging(rep);
}



void DialServ::miseAJour()
{
    QString text;
    socket->read((char *)(text.data()), sizeof(quint64));

    if (text == "")
        text = "Une nouvelle mise a jour est disponible.\r\nVoulez-vous lancer la mise a jour?";
    else
        text = "La mise a jour " + text +" est disponible.\r\nVoulez-vous lancer la mise a jour?";
    int ret = QMessageBox::question((QWidget *)this->parent(), "Mise a jour", text, QMessageBox::No, QMessageBox::Yes);
    if (ret == QMessageBox::Yes)
    {
        MAJour *maj = new MAJour((QWidget *)this->parent());
        maj->show();
    }
}

void DialServ::search(QString l)
{

    QByteArray paquet;
    QByteArray paquetSize;

    quint8 proto = SEARCHCONTACT;
    paquet.append(((char *)(&proto)), sizeof(quint8));
    chargeQString(&paquet, l);

    quint64 size = (quint64) paquet.size();
    paquetSize.append(((char *)(&size)), sizeof(quint64));
    socket->write(paquetSize);
    socket->write(paquet);
}

void DialServ::authentification()
{
    quint8 rep = dechargeQuint8();
    if (rep == OK)
    {
        user = dechargerUser();
        connected(true);
        int length = dechargeInt();

        for (int i = 0; i < length; i++)
        {


            User *c = dechargerFriend();
            addContact(c);

            user->potes.insert(i, c->log);
        }

  /*      QByteArray paquet;
        QByteArray paquetSize;

       quint8 proto = GETSTATE;
        paquet.append(((char *)(&proto)), sizeof(quint8));
        quint64 size = (quint64) paquet.size();
        paquetSize.append(((char *)(&size)), sizeof(quint64));
        socket->write(paquetSize);
        socket->write(paquet);
*/    }
    else
    {

        QString tmp = dechargeString();
        qDebug() << tmp;
        QMessageBox::warning((QWidget *)this->parent(), "Erreur", tmp);
        connectedToServ(true);
    }
}

void DialServ::inviteContact(QString log)
{
    if (user->potes.contains(log) || log == user->log)
        return;
    QByteArray paquet;
    QByteArray paquetSize;

    quint8 proto = INVITECONTACT;
    paquet.append(((char *)(&proto)), sizeof(quint8));
    chargeQString(&paquet, log);

    quint64 size = (quint64) paquet.size();
    paquetSize.append(((char *)(&size)), sizeof(quint64));
    socket->write(paquetSize);
    socket->write(paquet);
}


void DialServ::newInvite()
{
    User * u = dechargerFriend();
    if (QMessageBox::question((QWidget *)this->parent(),
                              tr("Demande d'ajout"),
                              u->log + tr(" voudrais vous ajouter a sa liste de contact."),
                              QMessageBox::Yes,
                              QMessageBox::No) == QMessageBox::Yes)
    {

        addContact(u);
        user->potes.insert(user->potes.count(), u->log);
        QByteArray paquet;
        QByteArray paquetSize;

        quint8 proto = INVITEOK;
        paquet.append(((char *)(&proto)), sizeof(quint8));
        chargeQString(&paquet, u->log);

        quint64 size = (quint64) paquet.size();
        paquetSize.append(((char *)(&size)), sizeof(quint64));
        socket->write(paquetSize);
        socket->write(paquet);
    }
}

void DialServ::inviteNotSended()
{
    QString log = dechargeString();
    QMessageBox::warning((QWidget *)this->parent(), tr("User introuvable"), tr("L'utilisateur ") + log + tr(" est introuvable."));
}
void DialServ::inviteSended()
{
    QString log = dechargeString();
    QMessageBox::information((QWidget *)this->parent(), tr(""), tr("La demande d'invitation a bien ete envoye a ") + log + tr("."));

}
void DialServ::inviteOk()
{
    User * u = dechargerFriend();

        addContact(u);
        user->potes.insert(user->potes.count(), u->log);

}

void DialServ::setState()
{
    User *c = dechargerFriend();

    modifyContact(c);
    stateChanged(*c);
}

void DialServ::changeStatus(int st)
{
    State s = (State)st;
    user->etat = s;

    QByteArray paquet;
    QByteArray paquetSize;

    quint8 proto = SETSTATE;
    paquet.append(((char *)(&proto)), sizeof(quint8));
    chargeUser(&paquet, user);

    quint64 size = (quint64) paquet.size();
    paquetSize.append(((char *)(&size)), sizeof(quint64));
    socket->write(paquetSize);
    socket->write(paquet);

}

void DialServ::changeUser()
{

    QByteArray paquet;
    QByteArray paquetSize;

    quint8 proto = SETSTATE;
    paquet.append(((char *)(&proto)), sizeof(quint8));
    chargeUser(&paquet, user);

    quint64 size = (quint64) paquet.size();
    paquetSize.append(((char *)(&size)), sizeof(quint64));
    socket->write(paquetSize);
    socket->write(paquet);

}
void DialServ::changeRang(QString st)
{
    user->rang = st;

    QByteArray paquet;
    QByteArray paquetSize;

    quint8 proto = SETSTATE;
    paquet.append(((char *)(&proto)), sizeof(quint8));
    chargeUser(&paquet, user);

    quint64 size = (quint64) paquet.size();
    paquetSize.append(((char *)(&size)), sizeof(quint64));
    socket->write(paquetSize);
    socket->write(paquet);
}



void DialServ::sendMsg(QString l, QString m)
{
    QByteArray paquet;
    QByteArray paquetSize;

    quint8 proto = MESSAGE;
    paquet.append(((char *)(&proto)), sizeof(quint8));
    chargeQString(&paquet, l);
    chargeQString(&paquet, m);
    quint64 size = (quint64) paquet.size();
    paquetSize.append(((char *)(&size)), sizeof(quint64));
    socket->write(paquetSize);
    socket->write(paquet);
}

void DialServ::rcvMsg()
{
    QString log = dechargeString();
    QString msg = dechargeString();
    newMsg(log, msg);
    qDebug() << "Receive message: " + msg + " From: " +log;
}

void DialServ::rcvPub()
{
    QString log = dechargeString();
    QString text = dechargeString();
    unsigned int h = QTime::currentTime().hour();
    unsigned int m = QTime::currentTime().minute();

    QString aff = "<font color=\"gray\">";
    if (text.isEmpty())
        return;
    if (m < 10)
      aff += QString::number(h) + ":0" + QString::number(m);
    else
        aff += QString::number(h) + ":" + QString::number(m);
    aff+= "<font><font color=\"red\"> ";
    aff += log + ":";
    aff += " <font><font color=\"black\"> ";
//    sendMessage(login, text);
    aff += text;
    showText(aff);

}

void DialServ::publicate()
{
/*    QByteArray paquet;
    QDataStream out(&paquet, QIODevice::WriteOnly);
    out << (quint64) 0;
    out << (quint8) PUBLICATION;

    out.device()->seek(0);
    out << (quint64) (paquet.size() - sizeof(quint64));
    socket->write(paquet);

    unsigned int h = QTime::currentTime().hour();
    unsigned int m = QTime::currentTime().minute();
    QString text;
    QString aff = "<font color=\"gray\">";
    if (text.isEmpty())
        return;
    if (m < 10)
      aff += QString::number(h) + ":0" + QString::number(m);
    else
      aff += QString::number(h) + ":" + QString::number(m);
    aff += "<font><font color=\"blue\"> ";
    aff += "You :";
    aff += " <font><font color=\"black\"> ";
    aff += text;
    showText(aff);
    */
}
