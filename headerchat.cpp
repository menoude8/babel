#include "headerchat.h"

HeaderChat::HeaderChat(User *u, QWidget *parent) :
    QGroupBox(parent), user(u)
{
    setFixedHeight(125);
//    QVBoxLayout *l = new QVBoxLayout(this);
  //  QWidget *zone = new QWidget(this);

    QGridLayout *layout = new QGridLayout();
    pix = new QLabel(this);
    if (u->avatar.isNull())
        pix->setPixmap(resizePic(QPixmap(":/assets/default-avatar.png"),80,80));
    else
        pix->setPixmap(resizePic(QPixmap::fromImage(u->avatar), 80, 80));

    pix->setFixedSize(80,80);
    layout->addWidget(pix,1, 0, 2, 1);
    //QVBoxLayout *firstCol = new QVBoxLayout;
    log = new QLabel(u->log, this);
    log->setObjectName("headerLabel");
    log->setStyleSheet("font: 16px; color: black; font-weight:bold;");
    close = new QPushButton(QIcon(":/assets/close.png"),"", this);
     close->setObjectName("closeButton");
   // close->setFixedSize(60, 40);
    connect(close, SIGNAL(clicked()), this, SIGNAL(Close()));
    //QHBoxLayout *rangLay = new QHBoxLayout;
    rangPic = new QLabel(this);
    rangPic->setPixmap(resizePic(QPixmap(":/assets/rangIcone.png"), 15, 15));
    rang = new QLabel(u->rang, this);
    rang->setStyleSheet("font: 14px; color: gray; font-weight:bold;");
    layout->addWidget(rangPic, 1, 1, 1, 1, Qt::AlignRight);
    layout->addWidget(rang, 1, 2, 1, 1, Qt::AlignLeft);
    state = new QLabel(this);
    statePic = new QLabel(this);
    switch (u->etat)
    {
    case OffLine:
        statePic->setPixmap(resizePic(QPixmap(":/assets/offline.png"), 20, 20));
        state->setText("Deconnecte");
    break;
    case OnLine:
        statePic->setPixmap(resizePic(QPixmap(":/assets/online.png"), 20, 20));
        state->setText("Connecte");
    break;
    case Busy:
        statePic->setPixmap(resizePic(QPixmap(":/assets/busy.png"), 20, 20));
        state->setText("Occupe");
    break;
    case Away:
        statePic->setPixmap(resizePic(QPixmap(":/assets/away.png"), 20, 20));
        state->setText("Absent");
    break;
    }
    layout->addWidget(statePic, 2, 1, 1, 1, Qt::AlignRight);
    layout->addWidget(state, 2, 2, 1, 1, Qt::AlignLeft);
    QHBoxLayout *l = new QHBoxLayout;
    l->addLayout(layout);

//    = new QLabel(u, this);
     QGridLayout *headLay = new QGridLayout;
     headLay->addWidget(log, 0, 0, 1, 2,  Qt::AlignTop);
     headLay->addWidget(close, 0, 1, 1, 1, Qt::AlignRight | Qt::AlignTop);
     QVBoxLayout *Vl = new QVBoxLayout;
     QWidget *w = new QWidget(this);
      layout->setMargin(0);
      l->setMargin(5);
     w->setLayout(l);
     Vl->addLayout(headLay);
     Vl->addWidget(w, 0, Qt::AlignLeft);
     Vl->setMargin(0);
     Vl->setSpacing(0);
     setLayout(Vl);

}
