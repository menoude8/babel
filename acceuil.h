#ifndef ACCEUIL_H
#define ACCEUIL_H

#include <QWidget>
#include <QtGui>
#include "proxywidget.h"
#include"IPluginWidget.h"
#include "xmlplugin.h"

class Acceuil : public QWidget
{
    Q_OBJECT
public:
    explicit Acceuil(QWidget *parent = 0);
    ~Acceuil()
    {
        std::map<IPluginWidget *, WidgetAttr>::iterator it = widgets.begin();
        for (;it != widgets.end(); ++it)
        {

            IPluginWidget * wd = (*it).first;
           ProxyWidget *p = widgetProxy[wd];
           (*it).second.x = p->positionX();
           (*it).second.y = p->positionY();

        }
        XMLPlugin::ecrireWidgets(widgets);
    }

signals:
    
public slots:


protected:


private:
    QGraphicsScene *scene;
    QGraphicsView *view;
    QLabel *title;
    QListWidget *area;

    std::map<IPluginWidget *, WidgetAttr > widgets;
    std::map<IPluginWidget *, ProxyWidget * > widgetProxy;

};

#endif // ACCEUIL_H
