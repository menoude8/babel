#include "contactlist.h"

ContactList::ContactList(User *u, QWidget *parent) :
    QWidget(parent), user(u)
{
    newCo = NULL;
    QVBoxLayout * layout = new QVBoxLayout(this);
    login = new QLabel(user->log);
    login->setStyleSheet("font: 16px; color: black; font-weight:bold;");
    login->setMinimumWidth(200);
    rang = new QLabel(user->rang);
    rang->setStyleSheet("font: 14px; color: gray; font-weight:bold;");
    rangL = new QLineEdit(this);
    rangL->setObjectName("rangL");
    rangB = new QPushButton("OK", this);
    rangB->setObjectName("rangB");
    etat = new QLabel(this);
    avatar = new QLabel(this);
    if (user->avatar.isNull())
        avatar->setPixmap(resizePic(QPixmap(":/assets/default-avatar.png"),100,100));
    else
        avatar->setPixmap(resizePic(QPixmap::fromImage(user->avatar), 100, 100));
    switch (user->etat)
    {
    case OffLine:
    etat->setPixmap(resizePic(QPixmap(":/assets/offline.png"),50, 50));
    break;
    case OnLine:
    etat->setPixmap(resizePic(QPixmap(":/assets/online.png"),50, 50));
    break;
    case Away:
    etat->setPixmap(resizePic(QPixmap(":/assets/away.png"),50, 50));
    break;
    case Busy:
    etat->setPixmap(resizePic(QPixmap(":/assets/busy.png"),50, 50));
    break;

    }

    liste = new QListWidget(this);
    connect(liste, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(newChat(QModelIndex)));
    add = new QPushButton(QIcon(":/assets/adduser.png"), "", this);

    add->setObjectName("addButton");
    connect(add, SIGNAL(clicked()), this, SLOT(addClick()));
    status = new QComboBox(this);
    status->setObjectName("statusBox");
    status->addItem(QIcon(":/assets/offline.png"), "Hors ligne");
    status->addItem(QIcon(":/assets/online.png"), "En ligne");
    status->addItem(QIcon(":/assets/busy.png"), "Occupe");
    status->addItem(QIcon(":/assets/away.png"), "Absent");
    status->setCurrentIndex(1);
    connect(status, SIGNAL(currentIndexChanged(int)), this, SIGNAL(statusChanged(int)));
    connect(status, SIGNAL(currentIndexChanged(int)), this, SLOT(refreshStatus(int)));
    QGridLayout * header = new QGridLayout();
     header->addWidget(avatar, 0, 0, 3, 1);
    header->addWidget(etat, 0, 1, 1, 1);
    header->addWidget(login, 0, 2, 1,1);
    header->addWidget(rang, 1, 1, 1,2);

    QHBoxLayout *rangLay = new QHBoxLayout;
    rangLay->addWidget(rangL);
    rangLay->addWidget(rangB);
    rangLay->setSpacing(0);
    header->addLayout(rangLay, 2, 1, 1,2);
    connect(rangB, SIGNAL(clicked()), this, SLOT(setRang()));
    connect(rangL, SIGNAL(returnPressed()), this, SLOT(setRang()));
    QGroupBox *headerBox = new QGroupBox(this);
    headerBox->setLayout(header);
    headerBox->setObjectName("headerUser");
    QHBoxLayout * lay = new QHBoxLayout();

    lay->addWidget(status, 0, Qt::AlignHCenter);
    layout->addWidget(headerBox);
    layout->addLayout(lay);
    layout->addWidget(liste);
    layout->addWidget(add, 0, Qt::AlignHCenter);
    layout->setMargin(0);
    layout->setSpacing(0);
    setLayout(layout);
}

