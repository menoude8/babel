#ifndef ADDCONTACT_H
#define ADDCONTACT_H

#include <QWidget>
#include <QtGui>

class AddContact : public QWidget
{
    Q_OBJECT
public:
    explicit AddContact(QWidget *parent = 0);

signals:
    void Cancel();
    void Add(QString);
public slots:
    void bottonOkClick()
    {
        if (login->text() != "")
            Add(login->text());
    }
    void refreshBut()
    {
        if (login->text() == "")
        {
            ok->setEnabled(false);
        }
        else
        {
            ok->setEnabled(true);
        }
    }

private:
    QLabel *titre;
    QLineEdit *login;
    QPushButton *ok;
    QPushButton *cancel;
};

#endif // ADDCONTACT_H
