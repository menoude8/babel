#ifndef CALLCONFIRMATION_H
#define CALLCONFIRMATION_H

#include <QtGui>

class CallConfirmation : public QDialog
{
    Q_OBJECT
public:
    explicit CallConfirmation(QString log, QString i, quint16 p, QWidget *parent = 0);
    
signals:
    void acceptCall(QString ip, quint16 port);
    void refuseCall(QString ip, quint16 port);
    void acceptCallWithVideo(QString ip, quint16 port);

public slots:
    void refuseSlot()
    {
        close();
        refuseCall(ip, port);
    }
    void acceptSlot()
    {
        close();
        acceptCall(ip, port);
    }
    void acceptWVSlot()
    {
        close();
        acceptCallWithVideo(ip, port);
    }

private:
    QPushButton *ok;
    QPushButton *okWithVideo;
    QPushButton *ko;
    QLabel *title;
    QString ip;
    quint16 port;
};

#endif // CALLCONFIRMATION_H
